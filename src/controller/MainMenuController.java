package controller;

import controller.delegates.ISceneControllerDelegate;
import controller.handler.MainMenuEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import model.constants.Music;
import model.interfaces.IEventPublisher;
import model.manager.SoundManager;

import java.io.IOException;

public class MainMenuController extends Controller
{
    public MainMenuController(ISceneControllerDelegate delegate)
    {
        super(delegate);
        loadScene();
        init();
    }
    
    @Override
    protected void init()
    {
        ((IEventPublisher)getController()).registerEventHandler(new MainMenuEventHandler(getDelegate()));
        SoundManager.playMusic(Music.BACKGROUNDMUSICMAINMENU);
    }
    
    @Override
    protected void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/MainMenu.fxml"));
            AnchorPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
