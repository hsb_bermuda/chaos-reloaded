package controller;


import controller.delegates.ISceneControllerDelegate;
import controller.handler.MultiplayerMenuEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import view.MultiplayerMenuViewController;

import java.io.IOException;

public class MultiplayerMenuController extends Controller
{
    public MultiplayerMenuController(ISceneControllerDelegate delegate)
    {
        super(delegate);
        loadScene();
        init();
    }
    
    @Override
    protected void init()
    {
        ((MultiplayerMenuViewController)getController()).registerEventHandler(new MultiplayerMenuEventHandler(getDelegate()));
    }
    
    @Override
    protected void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/MultiplayerMenu.fxml"));
            AnchorPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

}
