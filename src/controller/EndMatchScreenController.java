package controller;

import controller.delegates.ISceneControllerDelegate;
import controller.handler.EndMatchScreenEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import model.constants.Sound;
import model.interfaces.IEventPublisher;
import model.manager.SoundManager;

import java.io.IOException;

public class EndMatchScreenController extends Controller
{
    public EndMatchScreenController(ISceneControllerDelegate delegate)
    {
        super(delegate);
        loadScene();
        init();
    }

    @Override
    public void init()
    {
        ((IEventPublisher)getController()).registerEventHandler(new EndMatchScreenEventHandler(getDelegate()));
        SoundManager.playSound(Sound.VICOTRYSOUND);
    }

    @Override
    public void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/EndMatchScreen.fxml"));
            AnchorPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
