package controller;

import controller.delegates.ISceneControllerDelegate;

import controller.handler.SettingEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import model.interfaces.IEventPublisher;

import java.io.IOException;

public class SettingsController extends Controller
{

    public SettingsController(ISceneControllerDelegate delegate) {
        super(delegate);
        loadScene();
        init();
    }
    
    @Override
    protected void init()
    {
        ((IEventPublisher)getController()).registerEventHandler(new SettingEventHandler(getDelegate()));
    }
    
    @Override
    protected void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/Settings.fxml"));
            AnchorPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
