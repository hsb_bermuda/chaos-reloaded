package controller;

import controller.delegates.ISceneControllerDelegate;
import controller.handler.MatchEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import model.Match;
import model.constants.Music;
import model.manager.MatchManager;
import model.manager.SoundManager;
import view.MatchViewController;

import java.io.IOException;

public class MatchController extends Controller
{
    private Match match;

    public MatchController(ISceneControllerDelegate delegate)
    {
        super(delegate);

        Match match = MatchManager.getInstance().getMatch();

        loadScene();
        this.match = match;
        init();
    }
    
    @Override
    public void init()
    {
        MatchEventHandler eventHandler = new MatchEventHandler(getDelegate(), match, (MatchViewController) getController());
        ((MatchViewController)getController()).registerEventHandler(eventHandler);
        ((MatchViewController)getController()).update(match);
        SoundManager.playMusic(Music.BACKGROUNDMUSICMATCH);
    }
    
    @Override
    protected void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/MatchView.fxml"));
            BorderPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
