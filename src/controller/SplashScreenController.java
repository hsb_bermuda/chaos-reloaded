package controller;

import model.interfaces.ILoadingScreenEventHandler;
import controller.delegates.ISceneControllerDelegate;
import controller.handler.FileManagerEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import model.GameState;
import model.Match;
import model.manager.EnvironmentManager;
import model.manager.FileManager;
import model.manager.MatchManager;
import model.manager.NetworkingManager;

import java.io.IOException;

public class SplashScreenController extends Controller implements ILoadingScreenEventHandler
{
    public SplashScreenController(ISceneControllerDelegate delegate)
    {
        super(delegate);
        loadScene();
        loadGameFiles();
    }

    public SplashScreenController(ISceneControllerDelegate delegate, boolean skipLoadingFiles)
    {
        super(delegate);
        loadScene();

        NetworkingManager.getInstance().getNetworkEventHandler().registerLoadingScreenEventHandler(this);
    }
    
    @Override
    protected void init()
    {
    }
    
    @Override
    protected void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/SplashScreen.fxml"));
            AnchorPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    public void loadGameFiles()
    {
        FileManagerEventHandler eventHandler = new FileManagerEventHandler(getDelegate());
        FileManager.getInstance().registerEventHandler(eventHandler);

        // Load files in a seperate Thread.
        Thread thread = new Thread(() -> FileManager.getInstance().loadFiles());
        thread.start();
    }

    @Override
    public void exchangeMatch(Match match)
    {
        MatchManager.getInstance().setMatch(match);
        EnvironmentManager.getInstance().init(match.getEnvironment());
    }

    @Override
    public void startGame()
    {
        getDelegate().changeGameState(GameState.INGAME);
    }
}