package controller;

import controller.delegates.ISceneControllerDelegate;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import model.GameState;
import model.interfaces.ILobbyEventHandler;
import model.manager.MatchManager;
import model.manager.SoundManager;
import model.manager.NetworkingManager;
import utils.MatchInformation;
import view.MatchLobbyViewController;

import java.io.IOException;

public class MatchLobbyController extends Controller implements ILobbyEventHandler
{
    
    public MatchLobbyController(ISceneControllerDelegate delegate)
    {
        super(delegate);
        loadScene();
        init();
    }
    
    @Override
    protected void init()
    {
        ((MatchLobbyViewController)getController()).registerEventHandler(this);
    }
    
    @Override
    protected void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/MatchLobby.fxml"));
            AnchorPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    @Override
    public void back()
    {
        getDelegate().changeGameState(GameState.MAINMENU);
    }
    
    @Override
    public void startMatch(MatchInformation matchInformation)
    {
        MatchManager.getInstance().initMatch(matchInformation);
        SoundManager.stopMusic();
        
        getDelegate().changeGameState(GameState.INGAME);
    }

    @Override
    public void hostMatch(MatchInformation matchInformation, String serverName, int port)
    {
        MatchManager.getInstance().initMatch(matchInformation);
        NetworkingManager.getInstance().setServer(serverName, port);

        getDelegate().changeGameState(GameState.HOSTLOBBY);
    }
}
