package controller;

import model.interfaces.IServerEventHandler;
import controller.delegates.ISceneControllerDelegate;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import model.GameState;
import model.Match;
import model.interfaces.IMultiplayerLobbyEventHandler;
import model.manager.MatchManager;
import model.manager.NetworkingManager;
import networking.commands.Command;
import networking.commands.CommandType;
import networking.managers.DatabaseManager;
import networking.payloads.MatchPayload;
import networking.payloads.StartGamePayload;
import utils.ClientInformation;
import view.MultiplayerLobbyViewController;

import java.io.IOException;
import java.net.InetAddress;

public class MultiplayerLobbyController extends Controller implements IMultiplayerLobbyEventHandler, IServerEventHandler
{
    NetworkingManager networkingManager = NetworkingManager.getInstance();
    DatabaseManager databaseManager = DatabaseManager.getInstance();
    MatchManager matchManager = MatchManager.getInstance();

    public MultiplayerLobbyController(ISceneControllerDelegate delegate)
    {
        super(delegate);
        loadScene();
        init();
    }

    @Override
    protected void init()
    {
        ((MultiplayerLobbyViewController)getController()).registerEventHandler(this);
        networkingManager.getNetworkEventHandler().registerServerEventHandler(this);
    }

    @Override
    protected void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/MultiplayerLobby.fxml"));
            AnchorPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void back()
    {
        getDelegate().changeGameState(GameState.LOBBY);

    }

    @Override
    public void startMatch()
    {
        Match match = MatchManager.getInstance().getMatch();
        Command exchangeMatchCommand = new Command(CommandType.EXCHANGE_MATCH, new MatchPayload(match));
        NetworkingManager.getInstance().send(exchangeMatchCommand);
        Command startMatchCommand = new Command(CommandType.STARTGAME, new StartGamePayload());
        NetworkingManager.getInstance().send(startMatchCommand);

        getDelegate().changeGameState(GameState.INGAME);
    }


    @Override
    public void clientConnected(InetAddress ip, int port)
    {
        ClientInformation clientInformation = new ClientInformation(ip.getHostAddress(), port);
        ((MultiplayerLobbyViewController)getController()).addClient(clientInformation);
    }
}
