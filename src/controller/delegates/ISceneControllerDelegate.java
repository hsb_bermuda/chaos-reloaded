package controller.delegates;

import model.GameState;

/**
 * By implementing this interface, a controlled flow of information between the higher-order GameStageController and its child-ViewControllers is created.
 * The child-ViewControllers can only invoke methods specified in this interface.
 */
public interface ISceneControllerDelegate
{
    /**
     * Changes the state of the game.
     * @param state
     */
    void changeGameState(GameState state);
}
