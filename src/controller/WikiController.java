package controller;

import controller.delegates.ISceneControllerDelegate;
import controller.handler.WikiEventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import model.constants.Music;
import model.manager.SoundManager;
import view.WikiViewController;

import java.io.IOException;

public class WikiController extends Controller
{

    public WikiController(ISceneControllerDelegate delegate)
    {
        super(delegate);
        loadScene();
        init();
    }

    public void init()
    {
        ((WikiViewController)getController()).registerEventHandler(new WikiEventHandler(getDelegate()));
        SoundManager.playMusic(Music.BACKGROUNDMUSICWIKI);
    }

    public void loadScene()
    {
        try
        {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("view/Wiki.fxml"));
            BorderPane rootPane = fxmlLoader.load();
            setScene(new Scene(rootPane));
            setController(fxmlLoader.getController());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
