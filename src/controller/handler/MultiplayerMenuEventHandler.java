package controller.handler;

import controller.delegates.ISceneControllerDelegate;
import model.GameState;
import model.interfaces.IMultiplayerMenuEventHandler;
import model.manager.NetworkingManager;
import networking.commands.Command;
import networking.commands.CommandType;
import networking.payloads.DebugPayload;

public class MultiplayerMenuEventHandler implements IMultiplayerMenuEventHandler
{
    private ISceneControllerDelegate delegate;
    NetworkingManager networkingManager = NetworkingManager.getInstance();

    public MultiplayerMenuEventHandler(ISceneControllerDelegate delegate)
    {
        this.delegate = delegate;
    }
    
    @Override
    public void joinMatch(String address, int port)
    {
        networkingManager.setClient(address, port);

        delegate.changeGameState(GameState.LOADINGSCREEN);
    }
    
    @Override
    public void back()
    {
        delegate.changeGameState(GameState.MAINMENU);
    }
}
