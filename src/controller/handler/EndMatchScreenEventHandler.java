package controller.handler;

import controller.delegates.ISceneControllerDelegate;
import model.GameState;
import model.interfaces.IEndMatchScreenEventHandler;

/**
 * Handles events coming from the end screen of the game.
 */
public class EndMatchScreenEventHandler implements IEndMatchScreenEventHandler
{
    private ISceneControllerDelegate delegate;

    public EndMatchScreenEventHandler(ISceneControllerDelegate delegate)
    {
        this.delegate =delegate;
    }

    @Override
    public void backToMainMenu()
    {
        delegate.changeGameState(GameState.MAINMENU);
    }
}
