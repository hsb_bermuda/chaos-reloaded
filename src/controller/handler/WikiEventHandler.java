package controller.handler;

import controller.delegates.ISceneControllerDelegate;
import javafx.application.Platform;
import model.GameState;
import model.interfaces.IWikiEventHandler;
import model.manager.SoundManager;

public class WikiEventHandler implements IWikiEventHandler
{
    ISceneControllerDelegate delegate;
    public WikiEventHandler(ISceneControllerDelegate delegate)
    {
        this.delegate = delegate;
    }

    @Override
    public void goToMainMenu()
    {
        SoundManager.stopMusic();
        delegate.changeGameState(GameState.MAINMENU);
    }

    @Override
    public void exit()
    {
        Platform.exit();
    }

    @Override
    public void goToWiki()
    {
        delegate.changeGameState(GameState.WIKI);
    }
}
