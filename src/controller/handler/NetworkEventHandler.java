package controller.handler;


import model.interfaces.IClientEventHandler;
import model.interfaces.ILoadingScreenEventHandler;
import model.interfaces.IServerEventHandler;
import javafx.application.Platform;
import model.Match;
import model.interfaces.IMatchEventHandler;
import networking.commands.Command;
import networking.interfaces.ClientListener;
import networking.interfaces.Receiver;
import networking.interfaces.ServerListener;
import utils.CommandDecomposer;

import java.net.InetAddress;

public class NetworkEventHandler implements ServerListener, ClientListener, Receiver
{
    private final String debugPrefix = "[Network] ";

    private IServerEventHandler serverEventHandler;
    private IClientEventHandler clientEventHandler;
    private ILoadingScreenEventHandler loadingScreenEventHandler;
    private IMatchEventHandler matchEventHandler;

    @Override
    public void clientConnected(InetAddress ip, int port)
    {
        serverEventHandler.clientConnected(ip, port);
    }

    @Override
    public void clientDisconnected(InetAddress ip, int port)
    {
        System.out.println(debugPrefix+ "Client disconnected. IP: " + ip);
    }

    @Override
    public void couldNotConnectToNewClient() {
        System.out.println(debugPrefix + "Could not connect to new client.");
    }

    @Override
    public void unknownHost()
    {
        System.out.println(debugPrefix+ "Unknown host.");
    }

    @Override
    public void couldNotConnect()
    {
        System.out.println(debugPrefix+ "Could not connect to server.");
    }

    @Override
    public void serverClosed()
    {
        System.out.println(debugPrefix+ "Server closed.");
    }

    @Override
    public void disconnected()
    {
        System.out.println(debugPrefix+ "Disconnected.");
    }

    @Override
    public void connectedToServer()
    {
        System.out.println(debugPrefix+ "Connected to server.");
    }

    @Override
    public void receivedInput(Command command) {
        CommandDecomposer decomposer = new CommandDecomposer();

        switch(command.getCommandType()){
            case DEBUG:
                System.out.println(decomposer.decomposeDebugCommand(command).getDebugMessage());
                break;
            case STARTGAME:
                Platform.runLater(() -> loadingScreenEventHandler.startGame());
                break;
            case EXCHANGE_MATCH:
                Match match = decomposer.decomposeExchangeMatchCommand(command).getMatch();
                loadingScreenEventHandler.exchangeMatch(match);
                break;
            case EXCHANGE_MATCH_DELTA:
                Match matchDelta = decomposer.decomposeExchangeMatchDeltaCommand(command).getMatch();
                matchEventHandler.exchangeMatch(matchDelta);
                break;
            case ENDTURN:
                matchEventHandler.endTurn();
                break;
            default:
                break;
        }

    }

    public void registerMatchEventHandler(IMatchEventHandler matchEventHandler)
    {
        this.matchEventHandler = matchEventHandler;
    }

    public void registerLoadingScreenEventHandler(ILoadingScreenEventHandler loadingScreenEventHandler)
    {
        this.loadingScreenEventHandler = loadingScreenEventHandler;
    }

    public void registerServerEventHandler(IServerEventHandler serverEventHandler)
    {
        this.serverEventHandler = serverEventHandler;
    }

    public void registerClientEventHandler(IClientEventHandler clientEventHandler)
    {
        this.clientEventHandler = clientEventHandler;
    }
}
