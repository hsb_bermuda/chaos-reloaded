package controller.handler;

import controller.delegates.ISceneControllerDelegate;
import javafx.application.Platform;
import model.interfaces.IMainMenuEventHandler;
import model.GameState;
import model.manager.FileManager;
import model.manager.SoundManager;

/**
 * Handles all events regarding the MainMenu.
 */
public class MainMenuEventHandler implements IMainMenuEventHandler
{
    private ISceneControllerDelegate delegate;
    private SoundManager soundManager;
    
    public MainMenuEventHandler(ISceneControllerDelegate delegate)
     {
        this.delegate = delegate;
        soundManager = SoundManager.getInstance();
    }
    
    /**
     * Is called when a match should be started.
     */
    @Override
    public void startMatch()
    {
        delegate.changeGameState(GameState.LOBBY);
    }
    
    /**
     * Is called when the Wiki should be opened.
     */
    @Override
    public void openWiki()
    {
        SoundManager.stopMusic();
        delegate.changeGameState(GameState.WIKI);
    }
    
    @Override
    public void openSettings()
    {
        delegate.changeGameState(GameState.SETTINGS);
    }
    
    @Override
    public void openMultiplayerMenu()
    {
        delegate.changeGameState(GameState.MULTIPLAYERMENU);
    }
    
    /**
     * Is called when the game should be exited.
     */
    @Override
    public void exitGame()
    {
        FileManager.getInstance().changeConfigValues("musicVolume", SoundManager.getMusicPlayer().getVolume());
        FileManager.getInstance().changeConfigValues("soundVolume", SoundManager.getSoundEffectPlayer().getVolume());
        Platform.exit();
    }
}
