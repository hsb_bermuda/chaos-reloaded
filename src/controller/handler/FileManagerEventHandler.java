package controller.handler;

import controller.delegates.ISceneControllerDelegate;
import javafx.application.Platform;
import model.interfaces.IFileManagerEventHandler;
import model.GameState;

/**
 * Handles FileManager events.
 */
public class FileManagerEventHandler implements IFileManagerEventHandler
{
    /**
     * Provides possibility to request a scene change from this EventHandler.
     */
    private ISceneControllerDelegate delegate;
    
    public FileManagerEventHandler(ISceneControllerDelegate delegate)
    {
        this.delegate = delegate;
    }
    
    @Override
    public void filesLoaded()
    {
        Platform.runLater(() -> delegate.changeGameState(GameState.MAINMENU));
    }
}
