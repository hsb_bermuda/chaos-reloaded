package controller.handler;

import controller.delegates.ISceneControllerDelegate;
import model.GameState;
import model.interfaces.ISettingEventHandler;
import model.manager.SoundManager;

public class SettingEventHandler implements ISettingEventHandler
{
    ISceneControllerDelegate delegate;

    public SettingEventHandler(ISceneControllerDelegate delegate)
    {
        this.delegate = delegate;
    }

    @Override
    public void backToMainMenu()
    {
        SoundManager.stopMusic();
        delegate.changeGameState(GameState.MAINMENU);
    }
}
