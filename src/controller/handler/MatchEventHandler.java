package controller.handler;

import controller.delegates.ISceneControllerDelegate;
import javafx.application.Platform;
import model.*;
import model.actionresults.AttackResult;
import model.actionresults.BuyResult;
import model.actionresults.InteractionResult;
import model.cards.Card;
import model.interfaces.*;
import model.manager.*;
import networking.commands.Command;
import networking.commands.CommandType;
import networking.payloads.EndTurnPayload;
import networking.payloads.MatchPayload;
import utils.MatchType;
import view.MatchViewController;

public class MatchEventHandler implements IMatchEventHandler
{
    private Match match;
    private MatchViewController view;
    private ISceneControllerDelegate delegate;
    
    public MatchEventHandler(ISceneControllerDelegate delegate, Match match, MatchViewController view)
    {
        this.delegate = delegate;
        this.match = match;
        this.view = view;

        NetworkingManager.getInstance().getNetworkEventHandler().registerMatchEventHandler(this);
    }
    
    @Override
    public void onTileSelect(Vector2D position)
    {
        Creature creatureAtTileSelection = match.getEnvironment().getCreatureAtPosition(position);
        Creature selectedCreature = match.getSelectedCreature();
        if (creatureAtTileSelection != null)
        {
            if (creatureAtTileSelection.getOwner() == match.getActivePlayer())
            {
                onSelectedCreatureChanged(creatureAtTileSelection);
            } else if (selectedCreature != null)
            {
                onCreatureAttack(creatureAtTileSelection, selectedCreature);
            }
        } else if (match.getSelectedCard() != null)
        {
            onCardBuy(match.getSelectedCard(), position);
        } else
        {
            if (selectedCreature != null)
            {
                onCreatureMove(selectedCreature, position);
                if (selectedCreature.getPosition().equals(position))
                {
                    MapObject mapObject = match.getEnvironment().getMapObjectAtPosition(position);
                    if (mapObject instanceof IInteractable)
                    {
                        onMapObjectUsed((IInteractable) mapObject, selectedCreature);
                    }
                }
            }
        }
        view.update(match);
    }
    
    @Override
    public void onCardSelect(int cardIndex)
    {
        match.setSelectedCard(match.getActivePlayer().getDeck().get(cardIndex));
        view.updateDeck(match.getActivePlayer().getDeck(), match.getSelectedCard());
    }
    
    @Override
    public void onEndTurn()
    {
        synchronize();
        match.endTurn();
        resetSelection();
        view.update(match);
    }
    
    @Override
    public void resetSelection()
    {
        match.setSelectedCreature(null);
        match.setSelectedCard(null);
        view.update(match);
    }
    
    @Override
    public void onBeginHover(Vector2D position)
    {
        Creature hoveredCreature = match.getEnvironment().getCreatureAtPosition(position);
        if (hoveredCreature == match.getSelectedCreature())
            hoveredCreature = null;
        view.updateHoveredCreatureInfoControl(hoveredCreature);
        view.updateTileInfo(match.getEnvironment(), position);
    }
    
    @Override
    public void onSelectedCreatureChanged(Creature newCreature)
    {
        match.setSelectedCreature(newCreature);
        view.updateSelectedCreatureInfoControl(newCreature);
    }
    
    @Override
    public void onCreatureAttack(IDamagable target, IDamaging source)
    {
        AttackResult result = BattleManager.getInstance().attackObject(source, target);
        IDamagable resultTarget = result.getTarget();
        if(resultTarget.isDead())
        {
            if(resultTarget instanceof Entity)
            {
                if (result.getTarget() instanceof Wizard)
                {
                    MatchManager.getInstance().setResult(((Wizard) resultTarget).getOwner());
                    System.out.println(MatchManager.getInstance().getLosingPlayer().getUserName());
                    onMatchEnded();
                }

                match.getEnvironment().removeEntity((Entity)resultTarget);
            }
        }
    }
    
    @Override
    public void onCardBuy(Card card, Vector2D position)
    {
        BuyResult result = match.getActivePlayer().buyCard(match.getSelectedCard(), position);
        if(result.isSuccess())
            resetSelection();
    }
    
    @Override
    public void onCreatureMove(IMovable creature, Vector2D targetPosition)
    {
        creature.move(targetPosition);
    }
    
    @Override
    public void onMapObjectUsed(IInteractable object, Creature creature)
    {
        InteractionResult result = object.interact(creature);
        if(result.isSuccess() && result.getInteractable() instanceof ManaSource)
        {
            match.getEnvironment().removeEntity((Entity) result.getInteractable());
        }
    }
    
    @Override
    public void onMatchEnded()
    {
        // Reset the managers so a new match can begin.
        EnvironmentManager.getInstance().reset();
        ManaManager.getInstance().reset();
        delegate.changeGameState(GameState.ENDMATCHSCREEN);
    }
    
    @Override
    public void exchangeMatch(Match match)
    {
        this.match = match;

        Platform.runLater(() -> {
            resetSelection();
            view.update(match);
        });
    }

    @Override
    public void endTurn()
    {
        Platform.runLater(() -> {
            match.endTurn();
            resetSelection();
            view.update(match);
        });
    }

    @Override
    public void goToMainMenu()
    {
        SoundManager.stopMusic();
        delegate.changeGameState(GameState.MAINMENU);
    }

    @Override
    public void exit()
    {
        Platform.exit();
    }

    @Override
    public void goToWiki()
    {
        SoundManager.stopMusic();
        delegate.changeGameState(GameState.WIKI);
    }

    private void synchronize() {
        if (match.getMatchType() == MatchType.ONLINE) {

            Command deltaCommand = new Command(CommandType.EXCHANGE_MATCH_DELTA, new MatchPayload(match));
            NetworkingManager.getInstance().send(deltaCommand);

            Command endCommand = new Command(CommandType.ENDTURN, new EndTurnPayload());
            NetworkingManager.getInstance().send(endCommand);
        }
    }
}