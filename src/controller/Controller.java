package controller;

import controller.delegates.ISceneControllerDelegate;
import javafx.scene.Scene;
import view.ViewController;

public abstract class Controller
{
    private ISceneControllerDelegate delegate;
    private Scene scene;
    private ViewController controller;
    
    public Controller(ISceneControllerDelegate delegate)
    {
        setDelegate(delegate);
    }
    
    protected abstract void init();
    
    protected abstract void loadScene();
    
    public Scene getScene()
    {
        return scene;
    }
    
    public ViewController getController()
    {
        return controller;
    }
    
    public ISceneControllerDelegate getDelegate()
    {
        return delegate;
    }
    
    public void setScene(Scene scene)
    {
        this.scene = scene;
    }
    
    public void setController(ViewController controller)
    {
        this.controller = controller;
    }
    
    public void setDelegate(ISceneControllerDelegate delegate)
    {
        this.delegate = delegate;
    }
}
