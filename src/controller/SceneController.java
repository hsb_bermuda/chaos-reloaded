package controller;

import controller.delegates.ISceneControllerDelegate;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.Game;
import model.manager.MatchManager;
import model.GameState;
import model.manager.NetworkingManager;

public class SceneController implements ISceneControllerDelegate
{
    private Stage stage;
    
    private Controller activeController;
    private Game game = new Game();
    
    public SceneController(Stage stage)
    {
        this.stage = stage;
        stage.setResizable(false);
        stage.initStyle(StageStyle.DECORATED);
        // Load font.
        Font.loadFont(getClass().getResourceAsStream("/fonts/EnchantedLand.ttf"), 10);
        Font.loadFont(getClass().getResourceAsStream("/fonts/lbrite.ttf"), 10);

        stage.show();
    }
    
    public void runGame()
    {
        changeGameState(GameState.SPLASHSCREEN);
    }
    
    @Override
    public void changeGameState(GameState state)
    {
        game.setState(state);
        switch (state)
        {
            case SPLASHSCREEN:
                activeController = new SplashScreenController(this);
                break;
            case MAINMENU:
                activeController = new MainMenuController(this);
                break;
            case INGAME:
                activeController = new MatchController(this);
                break;
            case MULTIPLAYERMENU:
                activeController = new MultiplayerMenuController(this);
                break;
            case WIKI:
                activeController = new WikiController(this);
                break;
            case SETTINGS:
                activeController = new SettingsController(this);
                break;
            case LOBBY:
                activeController = new MatchLobbyController(this);
                break;
            case HOSTLOBBY:
                activeController = new MultiplayerLobbyController(this);
                break;
            case ENDMATCHSCREEN:
                activeController = new EndMatchScreenController(this);
                break;
            case LOADINGSCREEN:
                activeController = new SplashScreenController(this, true);
            default:
                break;
        }
        stage.setScene(activeController.getScene());
        stage.centerOnScreen();
    }
}