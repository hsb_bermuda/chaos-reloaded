package model;

import model.presets.CreaturePreset;

import java.io.Serializable;
import java.util.ArrayList;

public class Wizard extends Creature implements Serializable
{
    private ArrayList<Effect> activeEffects = new ArrayList<>();
    
    public Wizard(Vector2D position, Player owner, CreaturePreset creaturePreset)
    {
        super(position, owner, creaturePreset);
    }

    /**
     * Adds effect to wizard.
     * @param effect
     */
    public void addEffect(Effect effect)
    {
        activeEffects.add(effect);
    }

    /**
     * Removes effect from wizard.
     * @param effect
     */
    public void removeEffect(Effect effect)
    {
        activeEffects.remove(effect);
    }

    /**
     * Returns effects which are active on players wizard.
     * @return
     */
    public Effect[] getActiveEffects()
    {
        Effect[] arr = new Effect[activeEffects.size()];
        return activeEffects.toArray(arr);
    }
}
