package model;

import java.io.Serializable;

public enum MapSize implements Serializable
{
    SMALL("Small", 10),
    MEDIUM("Medium", 15),
    LARGE("Large", 20);
    
    private String label;
    private int size;

    MapSize(String label, int size)
    {
        this.label = label;
        this.size = size;
    }
    
    @Override
    public String toString()
    {
        return label;
    }

    public int getSize()
    {
        return size;
    }
}
