package model;

import model.manager.FileManager;
import model.presets.TilePreset;

import java.io.Serializable;

public class Tile implements Serializable
{
    private String key;
    private String name;
    private boolean isAccessible;
    private String description;
    private int height;

    public Tile(TilePreset preset, int height)
    {
        this.key = preset.getKey();
        this.name = preset.getName();
        this.isAccessible = preset.getIsAccessible();
        this.description = preset.getDescription();
        this.height = height;
    }

    /**
     * Returns the preset of a tile
     * @return
     */
    public TilePreset getPreset()
    {
        return FileManager.getInstance().getTilePresets().get(key);
    }
    
    public int getHeight()
    {
        return height;
    }

    public boolean isAccessible()
    {
        return isAccessible;
    }
    public String getDescription() {
        return description;
    }
}
