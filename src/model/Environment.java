package model;

import model.interfaces.IMovable;
import model.manager.BattleManager;

import java.io.Serializable;
import java.util.ArrayList;

public class Environment implements Serializable
{
    private Map map;
    private ArrayList<Creature> creatures;
    private ArrayList<MapObject> mapObjects;
    
    public Environment(Map map)
    {
        this.map = map;
        this.creatures = new ArrayList<>();
        this.mapObjects = new ArrayList<>();
    }
    
    /**
     * Returns the creature at a position on the map.
     * @param position
     * @return Null if there is no creature on the position.
     */
    public Creature getCreatureAtPosition(Vector2D position)
    {
        if(position == null)
            return null;
        for(Creature creature : creatures)
        {
            if(creature.getPosition().equals(position))
                return creature;
        }
        return null;
    }

    /**
     * Returns a map object an an specific position
     * @param position
     * @return
     */
    public MapObject getMapObjectAtPosition(Vector2D position)
    {
        if(position == null)
            return null;
        for(MapObject mapObject : mapObjects)
        {
            if(mapObject.getPosition().equals(position))
            {
                return mapObject;
            }
        }
        return null;
    }
    
    /**
     * Retrieves an array of creatures inside a given range.
     * @param position
     * @param range
     * @return
     */
    private ArrayList<Creature> getCreaturesInRange(Vector2D position, int range)
    {
        ArrayList<Creature> resList = new ArrayList<>();
        for(Creature creature : creatures)
        {
            if(position.distanceTo(creature.getPosition()) <= range)
                resList.add(creature);
        }
        return resList;
    }

    /**
     * Checks if a movable object is capable of moving to the specified position.
     * @param movableObject
     * @param targetPosition
     * @return False if the position is outside of the map.
     */
    public boolean canMoveToPosition(IMovable movableObject, Vector2D targetPosition)
    {
        // Check if target position is inside the map.
        if(!map.isInside(targetPosition) || map.getTile(targetPosition).isAccessible())
            return false;
        
        int neededRange = map.calculateMovementCost(movableObject.getPosition(), targetPosition);
        return movableObject.getMovementRange() >= neededRange;
    }
    
    /**
     * Activates all active effects of the given wizard and applies them to the creatures in the effect range.
     * @param wizard
     */
    public void activateEffectsOfWizard(Wizard wizard)
    {
        BattleManager battleManager = BattleManager.getInstance();
        for(Effect effect : wizard.getActiveEffects())
        {
            effect.setOwner(wizard.getOwner());
            ArrayList<Creature> creaturesInRange = getCreaturesInRange(effect.getPosition(), effect.getAttackRange());
            for (Creature creature : creaturesInRange)
            {
                if (creature.getOwner()!=wizard.getOwner())
                    battleManager.attackObject(effect, creature);

                effect.setActiveRounds(effect.getActiveRounds()+1);
                
                // Check if an effects duration is over and remove it from the effect list.
                if(effect.getActiveRounds() == effect.getPreset().getLastingRounds())
                    wizard.removeEffect(effect);
            }
        }
    }

    /**
     * Returns the type of highlighting on a specific map position
     * @param position
     * @param srcCreature
     * @return
     */
    public HighlightType getHighlight(Vector2D position, Creature srcCreature)
    {
        if(srcCreature == null)
            return null;
        Creature creatureOnPos = getCreatureAtPosition(position);
        if(creatureOnPos == srcCreature)
            return HighlightType.SELECTED;
        if(creatureOnPos != null)
        {
            if(creatureOnPos.getOwner() != srcCreature.getOwner())
                if(srcCreature.getPosition().distanceTo(creatureOnPos.getPosition()) <= srcCreature.getAttackRange())
                    return HighlightType.ATTACKABLE;
            else
                return null;
            else
                return HighlightType.FRIENDLY;
        }
        else if(srcCreature.getMovementPoints() >= srcCreature.getPosition().distanceTo(position))
            return HighlightType.MOVABLE;
        else
            return null;
    }

    /**
     * Refills left movement points.
     */
    public void refillLeftMovementCost()
    {
        for (Creature creature: creatures)
        {
            creature.setMovementPoints(creature.getMovementRange());
        }
    }

    /**
     * Resets the alreadyAttacked attribute.
     */
    public void resetHasAlreadyAttacked()
    {
        for (Creature creature: creatures)
        {
            creature.setHasAlreadyAttacked(false);
            if (creature instanceof Wizard)
            {
                for (Effect activeEffect : ((Wizard) creature).getActiveEffects())
                {
                    activeEffect.setHasAlreadyAttacked(false);
                }
            }
        }
    }

    /**
     * Removes entity from map.
     * @param entity
     */
    public void removeEntity(Entity entity)
    {
        boolean removeResult = creatures.remove(entity);
        if(!removeResult)
            mapObjects.remove(entity);
    }


    public void addCreature(Creature creature)
    {
        creatures.add(creature);
    }

    public void addMapObject(MapObject mapObject)
    {
        mapObjects.add(mapObject);
    }
    public void removeMapObject(MapObject mapObject)
    {
        mapObjects.remove(mapObject);
    }
    public boolean isInsideMap(Vector2D position)
    {
        return map.isInside(position);
    }
    public Map getMap()
    {
        return map;
    }
    
}
