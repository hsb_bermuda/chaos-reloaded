package model;

import model.constants.Sound;
import model.actionresults.InteractionResult;
import model.interfaces.IInteractable;
import model.manager.SoundManager;
import model.presets.MapObjectPreset;

import java.io.Serializable;

public class ManaSource extends MapObject implements IInteractable, Serializable
{
    private int additionalManaValue = 10;

    public ManaSource(Vector2D position, MapObjectPreset mapObjectPreset)
    {
        super(position, mapObjectPreset);
    }

    /**
     * Return the result of an interaction with a mana source map object.
     * @param creature
     * @return
     */
    @Override
    public InteractionResult interact(Creature creature)
    {
        creature.getOwner().setAvailableMana(creature.getOwner().getAvailableMana() + additionalManaValue);
        SoundManager.playSound(Sound.OWNMANASOUND);
        return new InteractionResult(true, this);
    }
    
    @Override
    public String toString()
    {
        return "Manasource: " + additionalManaValue;
    }
}