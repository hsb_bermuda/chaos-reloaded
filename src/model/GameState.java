package model;

import java.io.Serializable;

public enum GameState implements Serializable
{
    SPLASHSCREEN, MAINMENU, WIKI, INGAME, MULTIPLAYERMENU, SETTINGS, LOBBY, ENDMATCHSCREEN, LOADINGSCREEN, HOSTLOBBY
}
