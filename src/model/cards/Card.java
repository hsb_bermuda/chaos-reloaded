package model.cards;

import model.Faction;
import model.Player;
import model.Vector2D;
import model.interfaces.IIdentifiable;
import model.presets.PlayableObjectPreset;
import model.presets.Preset;

import java.io.Serializable;
import java.util.UUID;

public abstract class Card implements IIdentifiable, Serializable
{
    private final UUID uuid;
    private String name;

    Card(String name)
    {
        this.name = name;
        this.uuid = UUID.randomUUID();
    }

    public abstract void useCard(Player owner, Vector2D targetPosition);
    
    public String getName()
    {
        return name;
    }
    public abstract int getManaCost();
    public abstract Faction getFaction();
    public abstract PlayableObjectPreset getPreset();
    public UUID getUUID() { return uuid; }
}
