package model.cards;

import model.Faction;
import model.Player;
import model.Vector2D;
import model.constants.Sound;
import model.manager.EnvironmentManager;
import model.manager.FileManager;
import model.manager.SoundManager;
import model.presets.CreaturePreset;

import java.io.Serializable;

public class CreatureCard extends Card implements Serializable
{
    private int manaCost;
    private Faction faction;

    public CreatureCard(String name, CreaturePreset creaturePreset)
    {
        super(creaturePreset.getName());
        this.manaCost = creaturePreset.getManaCost();
        this.faction = creaturePreset.getFaction();
    }

    /**
     * Uses a CreatureCard and spawns the creature defined by the card.
     * @param owner
     * @param targetPosition
     */
    @Override
    public void useCard(Player owner, Vector2D targetPosition)
    {
        EnvironmentManager.getInstance().createCreature(owner, targetPosition, getPreset());
        SoundManager.playSound(Sound.SPAWNSOUND);
    }

    /**
     * Returns the preset of a creature card.
     * @return
     */
    @Override
    public CreaturePreset getPreset()
    {
        return FileManager.getInstance().getCreaturePresets().get(getName());
    }
    
    @Override
    public int getManaCost()
    {
        return manaCost;
    }
    @Override
    public Faction getFaction()
    {
        return faction;
    }
}
