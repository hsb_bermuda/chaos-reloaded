package model.cards;

import model.Faction;
import model.Player;
import model.Vector2D;
import model.Effect;
import model.manager.FileManager;
import model.presets.EffectPreset;
import model.presets.SpellPreset;

import java.io.Serializable;

public class SpellCard extends Card implements Serializable
{
    private int manaCost;
    private Faction faction;

    private String spellPresetName;
    private String effectPresetName;

    public SpellCard(String name, SpellPreset spellPreset, EffectPreset effectPreset)
    {
        super(effectPreset.getName());
        this.manaCost = spellPreset.getManaCost();
        this.faction = spellPreset.getFaction();
        this.spellPresetName = spellPreset.getName();
        this.effectPresetName = effectPreset.getName();
    }

    /**
     * Uses a SpellCard and spawns an Effect on the map.
     * @param owner
     * @param targetPosition
     */
    @Override
    public void useCard(Player owner, Vector2D targetPosition)
    {
        owner.getWizard().addEffect(new Effect(targetPosition, getEffectPreset()));
    }

    public EffectPreset getEffectPreset()
    {
        return FileManager.getInstance().getEffectPresets().get(effectPresetName);
    }
    @Override
    public int getManaCost()
    {
        return manaCost;
    }
    @Override
    public Faction getFaction()
    {
        return faction;
    }
    @Override
    public SpellPreset getPreset()
    {
        return FileManager.getInstance().getSpellPresets().get(spellPresetName);
    }
}
