package model.actionresults;

import model.interfaces.IInteractable;

public class InteractionResult extends ActionResult
{
    private IInteractable interactable;
    
    public InteractionResult(boolean isSuccess, IInteractable interactable)
    {
        super(isSuccess);
        this.interactable = interactable;
    }
    
    public IInteractable getInteractable()
    {
        return interactable;
    }
}
