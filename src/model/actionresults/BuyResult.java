package model.actionresults;

import model.cards.Card;

public class BuyResult extends ActionResult
{
    private Card card;
    private int manaSpeant;
    
    public BuyResult(boolean isSuccess, Card card, int manaSpeant)
    {
        super(isSuccess);
        this.card = card;
        this.manaSpeant = manaSpeant;
    }
    
    public Card getCard()
    {
        return card;
    }
    
    public int getManaSpeant()
    {
        return manaSpeant;
    }
}
