package model.actionresults;

import model.Vector2D;
import model.interfaces.IMovable;

public class MoveResult extends ActionResult
{
    private IMovable movable;
    private Vector2D oldPosition;
    private Vector2D newPosition;
    
    public MoveResult(boolean isSuccess, IMovable movable, Vector2D oldPosition, Vector2D newPosition)
    {
        super(isSuccess);
        this.movable = movable;
        this.oldPosition = oldPosition;
        this.newPosition = newPosition;
    }
    
    public IMovable getMovable()
    {
        return movable;
    }
    
    public Vector2D getOldPosition()
    {
        return oldPosition;
    }
    
    public Vector2D getNewPosition()
    {
        return newPosition;
    }
}
