package model.actionresults;

import model.interfaces.IDamagable;
import model.interfaces.IDamaging;

public class AttackResult extends ActionResult
{
    private IDamagable target;
    private IDamaging source;
    private int damageDealt;

    public AttackResult(boolean isSuccess, IDamagable target, IDamaging source, int damageDealt)
    {
        super(isSuccess);
        this.target = target;
        this.source = source;
        this.damageDealt = damageDealt;
    }

    public IDamagable getTarget()
    {
        return target;
    }

    public IDamaging getSource()
    {
        return source;
    }

    public int getDamageDealt()
    {
        return damageDealt;
    }
}
