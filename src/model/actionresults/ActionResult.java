package model.actionresults;

public abstract class ActionResult
{
    private boolean isSuccess;
    
    public ActionResult(boolean isSuccess)
    {
        this.isSuccess = isSuccess;
    }
    
    public boolean isSuccess()
    {
        return isSuccess;
    }
}
