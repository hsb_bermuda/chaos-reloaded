package model;

import model.presets.MapObjectPreset;

import java.io.Serializable;

public class Obstacle extends MapObject implements Serializable
{
    public Obstacle(Vector2D position, MapObjectPreset preset)
    {
        super(position, preset);
    }
    
    @Override
    public String toString()
    {
        return "Obstacle";
    }
}
