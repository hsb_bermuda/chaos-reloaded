package model;

import java.io.Serializable;
import java.util.Random;

public class Vector2D implements Serializable
{
    private int x;
    private int y;

    public Vector2D(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int getX()
    {
        return x;
    }

    public int getY()
    {
        return y;
    }

    /**
     * Returns the x-, and y-offset vector.
     * @param xOffset
     * @param yOffset
     * @return
     */
    public Vector2D getOffset(int xOffset, int yOffset)
    {
        return new Vector2D(x + xOffset, y + yOffset);
    }

    /**
     * Substracts one vector from another.
     * @param v2
     * @return
     */
    private Vector2D subtract(Vector2D v2)
    {
        return new Vector2D(this.x - v2.x, this.y - v2.y);
    }

    /**
     * Returns the absolute value of a vector.
     * @return
     */
    private Vector2D abs()
    {
        return new Vector2D(Math.abs(this.x), Math.abs(this.y));
    }

    /**
     * Returns the distance to a specific vector.
     * @param v2
     * @return
     */
    public int distanceTo(Vector2D v2)
    {
        Vector2D offsetVector = (this.subtract(v2)).abs();
        return offsetVector.getX() + offsetVector.getY();
    }

    /**
     * Returns new randomly generated vector.
     * @param xMax
     * @param yMax
     * @return
     */
    public static Vector2D getRandom(int xMax, int yMax)
    {
        return new Vector2D((new Random()).nextInt(xMax), (new Random()).nextInt(yMax));
    }

    /**
     * checks if two vectors are equal.
     * @param v2
     * @return
     */
    public boolean equals(Vector2D v2)
    {
        return (this.getX() == v2.getX()) && (this.getY() == v2.getY());
    }

    @Override
    public String toString()
    {
        return getX() + "," + getY();
    }
}
