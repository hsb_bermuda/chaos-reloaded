package model;

import java.io.Serializable;

public enum HighlightType implements Serializable
{
    MOVABLE, ATTACKABLE, FRIENDLY, SELECTED
}
