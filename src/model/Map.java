package model;

import java.io.Serializable;

public class Map implements Serializable
{
    private Tile[][] tiles;
    
    public Map(Tile[][] tiles)
    {
        this.tiles = tiles;
    }

    /**
     * Calculates the cost of a movement from a source position to a destination.
     * @param source
     * @param destination
     * @return
     */
    public int calculateMovementCost(Vector2D source, Vector2D destination)
    {
        // TODO: Implement path finding.
        return source.distanceTo(destination);
    }
    
    /**
     * Checks if a position is inside of the map boundaries.
     * @param position
     * @return
     */
    public boolean isInside(Vector2D position)
    {
        return position.getX() < getWidth() && position.getX() >= 0
                && position.getY() < getHeight() && position.getY() >= 0;
    }
    
    public Tile getTile(Vector2D pos)
    {
        return getTile(pos.getX(), pos.getY());
    }
    public Tile getTile(int x, int y)
    {
        return tiles[y][x];
    }
    public int getHeight()
    {
        return tiles.length;
    }
    public int getWidth()
    {
        return tiles[0].length;
    }
}