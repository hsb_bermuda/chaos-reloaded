package model;

import model.interfaces.IDamaging;
import model.manager.FileManager;
import model.presets.EffectPreset;

import java.io.Serializable;

public class Effect extends Entity implements IDamaging, Serializable
{
    private String name;
    private int attackValue;
    private int attackRange;
    private int activeRounds;
    private Player owner;
    private boolean hasAlreadyAttacked;
    
    public Effect(Vector2D position, EffectPreset preset)
    {
        super(position);
        this.name = preset.getName();
        this.attackValue = preset.getAttackValue();
        this.attackRange = preset.getRange();
        activeRounds = 0;
    }
    
    public EffectPreset getPreset()
    {
        return FileManager.getInstance().getEffectPresets().get(name);
    }
    public int getActiveRounds()
    {
        return activeRounds;
    }
    public void setActiveRounds(int activeRounds)
    {
        this.activeRounds = activeRounds;
    }

    @Override
    public int getAttackValue()
    {
        return attackValue;
    }
    @Override
    public int getAttackRange()
    {
        return attackRange;
    }
    @Override
    public boolean hasAlreadyAttacked()
    {
        return hasAlreadyAttacked;
    }
    @Override
    public void setHasAlreadyAttacked(boolean hasAlreadyAttacked)
    {
        this.hasAlreadyAttacked = hasAlreadyAttacked;
    }

    public void setOwner(Player owner)
    {
        this.owner = owner;
    }
    public Player getOwner()
    {
        return owner;
    }
}
