package model;

import java.io.Serializable;

public class PowerBalance implements Serializable
{
    private static final int MAX_VAlUE = 50;

    private int value;

    /**
     * Changes faction => impact of specific attacks
     * @param faction
     * @param impact
     */
    public void changeValue(Faction faction, int impact)
    {
        switch (faction)
        {
            case Law:
                value += impact;
                if (value > MAX_VAlUE)
                    value = MAX_VAlUE;
                break;
            case Chaos:
                value -= impact;
                if (value < (-1 * MAX_VAlUE))
                    value = (-1 * MAX_VAlUE);
                break;
            case Neutral:
                break;
        }
    }
    
    public int getValue()
    {
        return value;
    }
}
