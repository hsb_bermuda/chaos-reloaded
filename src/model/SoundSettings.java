package model;

import javafx.beans.property.DoubleProperty;
import model.constants.SoundSetting;
import model.manager.FileManager;

import java.io.Serializable;
import java.util.HashMap;

import static model.constants.SoundSetting.MUSIC_VOLUME;
import static model.constants.SoundSetting.SOUND_VOLUME;

public class SoundSettings implements Serializable
{
    private HashMap<SoundSetting, DoubleProperty> settings = new HashMap<>();

    public SoundSettings()
    {
        Config config = FileManager.getInstance().getConfig();
        settings.put(MUSIC_VOLUME, config.getMusicVolume());
        settings.put(SOUND_VOLUME, config.getSoundVolume());
    }

    public HashMap<SoundSetting, DoubleProperty> getSettings()
    {
        return settings;
    }
}
