package model.manager;

import model.cards.Card;
import model.Player;
import model.PowerBalance;

import java.util.ArrayList;

/**
 * The ManaManager contains all operations regarding the currency system of the game.
 * The singleton has to be initialized with a reference to the current PowerBalance of the match.
 */
public class ManaManager
{
    private static ManaManager instance = new ManaManager();
    public static ManaManager getInstance()
    {
        if(instance == null)
            instance = new ManaManager();
        return instance;
    }
    
    private PowerBalance powerBalance;
    
    private ManaManager()
    {
    }
    
    public void init(PowerBalance powerBalance)
    {
        this.powerBalance = powerBalance;
    }
    
    public void reset()
    {
        powerBalance = null;
    }
    
    public void adjustPowerBalance(Card card)
    {
        powerBalance.changeValue(card.getFaction(), (card.getPreset()).getBalanceImpact());
    }
    
    /**
     * Checks if the player has enough mana to buy a certain card.
     * @param player
     * @param card
     * @return
     */
    public boolean canBuy(Player player, Card card)
    {
        return  (player.getAvailableMana() - calculateActualCardCost(card)) >= 0;
    }
    
    /**
     * Calculates the actual card cost including the impact of the PowerBalance.
     * @param card
     * @return
     */
    public int calculateActualCardCost(Card card)
    {
        switch (card.getFaction())
        {
            case Law:
                return card.getManaCost() - powerBalance.getValue();
            case Chaos:
                return card.getManaCost() + powerBalance.getValue();
            case Neutral:
                default:
                    return card.getManaCost();
        }
    }
    
    /**
     * Is called at the end of each round and gives each player a certain amount of mana.
     * @param players
     * @param amount
     */
    public void addRoundBonusToPlayers(ArrayList<Player> players, int amount)
    {
        for (Player player : players)
        {
            player.setAvailableMana(player.getAvailableMana() + amount);
        }
    }
}
