package model.manager;

import model.*;
import model.cards.Deck;
import model.presets.WandPreset;
import utils.DeckGenerator;
import utils.MapGenerator;
import utils.MatchInformation;
import utils.MatchType;

import java.util.ArrayList;
import java.util.Random;

/**
 * the MatchManager contains all operations regarding players and entities.
 * The initMatch() method has to be called to initialize a new Match.
 */
public class MatchManager
{
    private Match match;
    private MatchInformation matchInformation;
    private Config config = FileManager.getInstance().getConfig();
    private Player losingPlayer;

    private static MatchManager instance = new MatchManager();
    public static MatchManager getInstance()
    {
        if(instance == null)
            instance = new MatchManager();
        return instance;
    }
    
    private MatchManager()
    {
    }

    /**
     * This method creates a Match instance.
     * @return
     */
    public void initMatch(MatchInformation matchInformation)
    {
        this.matchInformation = matchInformation;
        match = initMatch();
    }


    private Match initMatch()
    {
        ArrayList<String> usernames = getUsernames();
        ArrayList<Player> playerList = new ArrayList<>();

        Map map = MapGenerator.generateMap(getMapSize());

        Environment env = new Environment(map);
        EnvironmentManager environmentManager = EnvironmentManager.getInstance();
        environmentManager.init(env);

        Object[] wandPresets = FileManager.getInstance().getWandPresets().values().toArray();
        Random random = new Random();
        int randomNumber = random.nextInt(wandPresets.length);
        WandPreset wand_1 = (WandPreset) wandPresets[randomNumber];
        randomNumber = random.nextInt(wandPresets.length);
        WandPreset wand_2 = (WandPreset) wandPresets[randomNumber];

        Deck deck_1 = DeckGenerator.generateDeck(getDeckSize(), wand_1);
        Deck deck_2 = DeckGenerator.generateDeck(getDeckSize(), wand_2);

        Player player_01 = new Player(usernames.get(0), deck_1);
        Player player_02 = new Player(usernames.get(1), deck_2);

        playerList.add(player_01);
        playerList.add(player_02);

        environmentManager.createMapObject(new Vector2D(1,3), FileManager.getInstance().getMapObjectPresets().get("Rock"));
        environmentManager.createMapObject(new Vector2D(4,3), FileManager.getInstance().getMapObjectPresets().get("Tree"));
        environmentManager.createMapObject(new Vector2D(8,8), FileManager.getInstance().getMapObjectPresets().get("ManaSource"));

        return new Match(playerList, env, getMaxRounds(), getMatchType());
    }

    public Match getMatch()
    {
        return match;
    }

    public void setMatch(Match match)
    {
        this.match = match;
    }

    private MatchType getMatchType()
    {
        if (matchInformation.getMatchType() == null) {
            return config.getDefaultMatchType();
        } else {
            return matchInformation.getMatchType();
        }
    }

    private MapSize getMapSize()
    {
        if (matchInformation.getMatchType() == null) {
            return config.getDefaultMapSize();
        } else {
            return matchInformation.getMapSize();
        }
    }

    private int getMaxRounds()
    {
        if (matchInformation.getMatchType() == null) {
            return config.getDefaultMaxRounds();
        } else {
            return matchInformation.getRounds();
        }
    }

    private int getDeckSize()
    {
        if (matchInformation.getMatchType() == null) {
            return config.getDefaultDeckSize();
        } else {
            return matchInformation.getDeckSize();
        }
    }

    private int getManaSourceSize()
    {
        if (matchInformation.getMatchType() == null) {
            return config.getDefaultManaSourceCount();
        } else {
            return matchInformation.getManaSourceSize();
        }
    }

    private ArrayList<String> getUsernames() {
        if (matchInformation.getUsernames() == null) {
            ArrayList<String> usernames = new ArrayList<>();
            usernames.add("Player1");
            usernames.add("Player2");
            return usernames;
        } else {
            return matchInformation.getUsernames();
        }
    }

    public void setResult(Player losingPlayer)
    {
        this.losingPlayer = losingPlayer;
    }

    public Player getLosingPlayer()
    {
        return losingPlayer;
    }
}
