package model.manager;

import javafx.scene.image.Image;
import model.Config;
import model.Faction;
import model.constants.FilePaths;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.IFileManagerEventHandler;
import model.presets.*;
import org.json.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

public class FileManager implements IEventPublisher
{
    private static FileManager instance;
    public static FileManager getInstance()
    {
        if(instance == null)
            instance = new FileManager();
        return instance;
    }

    private Config config;
    private HashMap<String, CreaturePreset> creaturePresets = new HashMap<>();
    private HashMap<String, EffectPreset> effectPresets = new HashMap<>();
    private HashMap<String, MapObjectPreset> mapObjectPresets = new HashMap<>();
    private HashMap<String, SpellPreset> spellPresets = new HashMap<>();
    private HashMap<String, TilePreset> tilePresets = new HashMap<>();
    private HashMap<String, WandPreset> wandPresets = new HashMap<>();
    private ResourceBundle soundResources;
    private JSONObject configJSON;
    
    private IFileManagerEventHandler eventHandler;
    
    private FileManager()
    {
    
    }
    
    public void loadFiles()
    {
        // Artificially slow down file loading for splash screen to show.
        try
        {
            Thread.sleep(1000);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        loadConfig();
        loadSoundFiles();
        loadPresets();
        eventHandler.filesLoaded();
    }
    
    private void loadConfig()
    {
        configJSON = null;
        try
        {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("resource/config.json"));
            configJSON = new JSONObject(((org.json.simple.JSONObject) obj).toJSONString());
        } catch (Exception loadingConfigException)
        {
            loadingConfigException.printStackTrace();
        }

        if (configJSON != null)
        {
            try
            {
                config = new Config(configJSON.getString("defaultMap"),
                        configJSON.getString("defaultMapSize"),
                        configJSON.getInt("defaultMaxRounds"),
                        configJSON.getString("defaultMatchType"),
                        configJSON.getInt("defaultManaSourceCount"),
                        configJSON.getInt("defaultDeckSize"),
                        configJSON.getString("wizardPreset"),
                        configJSON.getInt("wizardSpawnRange"),
                        configJSON.getInt("standardHealth"),
                        configJSON.getInt("manaRoundBonus"),
                        configJSON.getDouble("soundVolume"),
                        configJSON.getDouble("musicVolume"));
            }
            catch (JSONException configLoadingException)
            {
                configLoadingException.printStackTrace();
            }

        }
        else
        {
            System.err.println("No JSONObject found.");
        }
    }

    private void loadCreaturePresets()
    {
        JSONArray creaturePresets = null;
        try
        {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("resource/creaturePreset.json"));
            JSONObject obj2 = new JSONObject(((org.json.simple.JSONObject) obj).toJSONString());
            creaturePresets = obj2.getJSONArray("creaturePresets");
        } catch (IOException | ParseException loadingCreaturePresetsException)
        {
            loadingCreaturePresetsException.printStackTrace();
        }

        if (creaturePresets != null)
        {
            for (int c = 0; c < creaturePresets.length(); c++)
            {
                try
                {
                    JSONObject creatureJSON = (JSONObject) creaturePresets.get(c);
                    this.creaturePresets.put(
                            creatureJSON.getString("name"),
                            new CreaturePreset(
                                    creatureJSON.getString("name"),
                                    creatureJSON.getString("description"),
                                    creatureJSON.getInt("attack"),
                                    creatureJSON.getInt("defence"),
                                    creatureJSON.getInt("attackRange"),
                                    creatureJSON.getInt("movementRange"),
                                    creatureJSON.getInt("manaCost"),
                                    creatureJSON.getInt("climbHeight"),
                                    creatureJSON.getInt("balanceImpact"),
                                    Faction.valueOf(creatureJSON.getString("faction")),
                                    new Image(getClass().getResourceAsStream(
                                            FilePaths.texturePath+creatureJSON.getString("image_id")))));
                }
                catch (JSONException creaturePresetLoadingException)
                {
                    creaturePresetLoadingException.printStackTrace();
                }
            }

        }
        else
        {
            System.err.println("No JSONObject found.");
        }
    }

    private void loadEffectPresets()
    {
        JSONArray effectPresets = null;
        try
        {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("resource/effectPreset.json"));
            JSONObject obj2 = new JSONObject(((org.json.simple.JSONObject) obj).toJSONString());
            effectPresets = obj2.getJSONArray("effectPresets");
        } catch (IOException | ParseException loadingCreaturePresetsException)
        {
            loadingCreaturePresetsException.printStackTrace();
        }

        if (effectPresets != null)
        {
            for (int c = 0; c < effectPresets.length(); c++)
            {
                try
                {
                    JSONObject effectJSON = (JSONObject) effectPresets.get(c);
                    this.effectPresets.put(
                            effectJSON.getString("name"),
                            new EffectPreset(
                                    effectJSON.getString("name"),
                                    effectJSON.getString("description"),
                                    effectJSON.getInt("attackValue"),
                                    effectJSON.getInt("range"),
                                    effectJSON.getInt("lastingRounds"),
                                    null));
                }
                catch (JSONException effectPresetLoadingException)
                {
                    effectPresetLoadingException.printStackTrace();
                }
            }

        }
        else
        {
            System.err.println("No JSONObject found.");
        }
    }

    private void loadMapObjectPresets()
    {
        JSONArray mapObjectPresets = null;
        try
        {
            JSONParser parser =  new JSONParser();
            Object obj = parser.parse(new FileReader("resource/mapObjectPreset.json"));
            JSONObject obj2 = new JSONObject(obj.toString());
            mapObjectPresets = obj2.getJSONArray("mapObjectPresets");
        } catch (IOException | ParseException loadingMapObjectException)
        {
            loadingMapObjectException.printStackTrace();
        }

        if (mapObjectPresets != null)
        {
            for (int c = 0; c < mapObjectPresets.length(); c++)
            {
                try
                {
                    JSONObject mapObjectJSON = (JSONObject) mapObjectPresets.get(c);
                    this.mapObjectPresets.put(
                            mapObjectJSON.getString("name"),
                            new MapObjectPreset(
                                    mapObjectJSON.getString("name"),
                                    mapObjectJSON.getString("description"),
                                    new Image(getClass().getResourceAsStream(FilePaths.texturePath+mapObjectJSON.getString("image_id"))),
                                    mapObjectJSON.getBoolean("isObstacle")));
                }
                catch (JSONException mapObjectPresetLoadingException)
                {
                    mapObjectPresetLoadingException.printStackTrace();
                }
            }
        }
        else
        {
            System.err.println("No JSONObject found.");
        }
    }

    private void loadSpellPresets()
    {
        JSONArray spellPresets = null;
        try
        {
            JSONParser parser =  new JSONParser();
            Object obj = parser.parse(new FileReader("resource/spellPreset.json"));
            JSONObject obj2 = new JSONObject(obj.toString());
            spellPresets = obj2.getJSONArray("spellPresets");
        } catch (IOException | ParseException loadingMapObjectException)
        {
            loadingMapObjectException.printStackTrace();
        }

        if (spellPresets != null)
        {
            for (int c = 0; c < spellPresets.length(); c++)
            {
                JSONObject spellJSON = (JSONObject) spellPresets.get(c);
                try
                {
                    this.spellPresets.put(
                            spellJSON.getString("name"),
                            new SpellPreset(
                                    spellJSON.getString("name"),
                                    spellJSON.getString("description"),
                                    spellJSON.getInt("manaCost"),
                                    spellJSON.getInt("balanceImpact"),
                                    Faction.valueOf(spellJSON.getString("faction")),
                                    null));
                }
                catch (JSONException spellPresetLoadingException)
                {
                    spellPresetLoadingException.printStackTrace();
                }

            }
        }
        else
        {
            System.err.println("No JSONObject found.");
        }
    }

    private void loadTilePresets()
    {
        JSONArray tilePresets = null;
        try
        {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader("resource/tilePreset.json"));
            JSONObject obj2 = new JSONObject(((org.json.simple.JSONObject) obj).toJSONString());
            tilePresets = obj2.getJSONArray("tilePresets");
        } catch (IOException | ParseException loadingTilePresetsException)
        {
            loadingTilePresetsException.printStackTrace();
        }

        if (tilePresets != null)
        {
            for (int c = 0; c < tilePresets.length(); c++)
            {
                try
                {
                    JSONObject tileJSON = (JSONObject) tilePresets.get(c);
                    this.tilePresets.put(
                            tileJSON.getString("key"),
                            new TilePreset(
                                    tileJSON.getString("tileName"),
                                    tileJSON.getString("description"),
                                    tileJSON.getBoolean("is_accessible"),
                                    tileJSON.getString("key"),
                                    new Image(getClass().getResourceAsStream(FilePaths.texturePath + tileJSON.getString("image_id")))));
                }
                catch (JSONException tilePresetLoadingException)
                {
                    tilePresetLoadingException.printStackTrace();
                }
            }
        }
        else
        {
            System.err.println("No JSONObject found.");
        }

    }

    private void loadWandPresets()
    {
        JSONArray wandPresets = null;
        try
        {
            JSONParser parser = new JSONParser();
            Object wandObjects = parser.parse(new FileReader("resource/wandPreset.json"));
            JSONObject wands = new JSONObject(((org.json.simple.JSONObject) wandObjects).toJSONString());
            wandPresets = wands.getJSONArray("wandPresets");
        } catch (IOException | ParseException loadingWandPresetsException)
        {
            loadingWandPresetsException.printStackTrace();
        }

        if (wandPresets != null)
        {
            for (int c = 0; c < wandPresets.length(); c++)
            {
                try
                {
                    JSONObject wandJSON = (JSONObject) wandPresets.get(c);
                    this.wandPresets.put(
                            wandJSON.getString("name"),
                            new WandPreset(
                                    wandJSON.getString("name"),
                                    wandJSON.getString("description"),
                                    null,
                                    Faction.valueOf(wandJSON.getString("faction")),
                                    wandJSON.getInt("spellQuantity")));
                }
                catch (JSONException wandPresetLoadingException)
                {
                    wandPresetLoadingException.printStackTrace();
                }
            }
        }
        else
        {
            System.err.println("No JSONObject found.");
        }

    }

    private void loadPresets()
    {
        loadEffectPresets();
        loadCreaturePresets();
        loadMapObjectPresets();
        loadSpellPresets();
        loadTilePresets();
        loadWandPresets();
    }

    private void loadSoundFiles()
    {
        Locale.setDefault(new Locale("en"));
        this.soundResources = ResourceBundle.getBundle("sounds");
    }

    public void changeConfigValues(String name, double value)
    {
        configJSON.put(name, value);
        try (FileWriter file = new FileWriter("resource/config.json"))
        {
            file.write(configJSON.toString());

        } catch (Exception e)
        {
                e.printStackTrace();
        }
    }
    
    public HashMap<String, CreaturePreset> getCreaturePresets()
    {
        return creaturePresets;
    }
    public HashMap<String, EffectPreset> getEffectPresets() {
        return effectPresets;
    }
    public HashMap<String, MapObjectPreset> getMapObjectPresets() {
        return mapObjectPresets;
    }
    public HashMap<String, SpellPreset> getSpellPresets() {
        return spellPresets;
    }
    public HashMap<String, TilePreset> getTilePresets()
    {
        return tilePresets;
    }
    public HashMap<String, WandPreset> getWandPresets()
    {
        return wandPresets;
    }
    public Config getConfig()
    {
        return config;
    }
    public ResourceBundle getSoundResources()
    {
        return soundResources;
    }

    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (IFileManagerEventHandler)eventHandler;
    }
}
