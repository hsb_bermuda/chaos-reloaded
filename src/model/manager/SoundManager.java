package model.manager;

import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import model.SoundSettings;
import model.constants.Music;
import model.constants.Sound;

import java.io.File;
import java.util.ResourceBundle;

import static model.constants.SoundConstants.INITAL_MUSIC_VOLUME;
import static model.constants.SoundConstants.INITIAL_SOUND_VOLUME;
import static model.constants.SoundSetting.MUSIC_VOLUME;
import static model.constants.SoundSetting.SOUND_VOLUME;

public class SoundManager
{

    private static MediaPlayer soundEffectPlayer;
    private static MediaPlayer musicPlayer;

    private static SoundManager instance;
    public static SoundManager getInstance()
    {
        if(instance == null)
            instance = new SoundManager();
        return instance;
    }

    private SoundSettings settings = new SoundSettings();

    private SoundManager()
    {

    }
    
    public static void playSound(Sound sound)
    {
        getInstance().playSoundInternal(sound);
    }

    private void playSoundInternal(Sound soundToPlay)
    {
        ResourceBundle soundBundle = FileManager.getInstance().getSoundResources();
        String soundEffect = soundBundle.getString(soundToPlay.toString());
        Media sound = new Media(new File(soundEffect).toURI().toString());
        soundEffectPlayer = new MediaPlayer(sound);
        settings.getSettings().putIfAbsent(SOUND_VOLUME, INITIAL_SOUND_VOLUME);
        soundEffectPlayer.volumeProperty().bindBidirectional(settings.getSettings().get(SOUND_VOLUME));
        soundEffectPlayer.play();
    }

    public static void playMusic(Music music)
    {
        getInstance().startMusicInternal(music);
    }
    
    public void startMusicInternal(Music musicToPlay)
    {
        ResourceBundle soundBundle = FileManager.getInstance().getSoundResources();
        String musicPath = soundBundle.getString(musicToPlay.toString());
        Media music = new Media(new File(musicPath).toURI().toString());
        musicPlayer= new MediaPlayer(music);
        settings.getSettings().putIfAbsent(MUSIC_VOLUME, INITAL_MUSIC_VOLUME);
        musicPlayer.volumeProperty().bindBidirectional(settings.getSettings().get(MUSIC_VOLUME));
        musicPlayer.setCycleCount(MediaPlayer.INDEFINITE);
        musicPlayer.play();
    }

    public static void stopMusic()
    {
        getMusicPlayer().stop();
    }

    public static MediaPlayer getMusicPlayer()
    {
        return musicPlayer;
    }
    public static MediaPlayer getSoundEffectPlayer()
    {
        return soundEffectPlayer;
    }
}
