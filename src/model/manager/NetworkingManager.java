package model.manager;

import controller.handler.NetworkEventHandler;
import networking.Client;
import networking.managers.DatabaseManager;
import networking.managers.ServerInformation;
import networking.messages.NetworkMessage;
import networking.Server;
import networking.commands.Command;
import networking.interfaces.*;

public class NetworkingManager
{
    private static NetworkingManager instance;

    public synchronized static NetworkingManager getInstance() {
        if(instance == null)
        {
            instance = new NetworkingManager();
        }
        return instance;
    }

    private NetworkingManager() {
        networkEventHandler = new NetworkEventHandler();
    }

    private Sender sender;

    private NetworkEventHandler networkEventHandler;


    public void setServer(String serverName, int port) {
        Server server = new Server(port, networkEventHandler, networkEventHandler);

        ServerInformation serverInformation = new ServerInformation(serverName, server.getIp(), port);

        DatabaseManager.getInstance().addToServerList(serverInformation);

        this.sender = server;
    }

    public NetworkEventHandler getNetworkEventHandler()
    {
        return networkEventHandler;
    }

    public void setClient(String ip, int port) {
        this.sender = new Client(ip, port, networkEventHandler, networkEventHandler);
    }

    public void send(Command command) {
        NetworkMessage networkMessage = new NetworkMessage(command);
        sender.send(networkMessage);
    }
}
