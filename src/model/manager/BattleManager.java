package model.manager;

import model.actionresults.AttackResult;
import model.constants.Sound;
import model.interfaces.IDamagable;
import model.interfaces.IDamaging;

/**
 * The BattleManager handles the interaction between to object when they are attacking each other.
 */
public class BattleManager
{
    private static BattleManager instance;
    public static BattleManager getInstance()
    {
        if(instance == null)
            instance = new BattleManager();
        return instance;
    }
    
    private BattleManager()
    {
    }
    
    /**
     * Is called when a Damage-Source attacks a damagable object. If the defence of the receiver is greater than the damage of the attacker the damage is set to zero.
     * @param source
     * @param receiver
     */
    public AttackResult attackObject(IDamaging source, IDamagable receiver)
    {
        if (!(source.hasAlreadyAttacked()))
        {
            int distance = source.getPosition().distanceTo(receiver.getPosition());
            if (distance > source.getAttackRange())
            {
                return new AttackResult(false, receiver, source, 0);
            }

            int calculatedDamage = source.getAttackValue() - receiver.getDefence();
            if (calculatedDamage < 0)
                calculatedDamage = 0;
            receiver.receiveDamage(calculatedDamage);
            SoundManager.playSound(Sound.ATTACKSOUND);
            source.setHasAlreadyAttacked(true);
            return new AttackResult(true, receiver, source, calculatedDamage);
        }
        else
        {
            SoundManager.playSound(Sound.NOTATTACKSOUND);
            return new AttackResult(false, receiver, source, 0);
        }
    }
}
