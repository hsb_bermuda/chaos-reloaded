package model.manager;

import model.*;
import model.presets.CreaturePreset;
import model.presets.MapObjectPreset;

public class EnvironmentManager
{
    private static EnvironmentManager instance;
    public static EnvironmentManager getInstance()
    {
        if(instance == null)
            instance = new EnvironmentManager();
        else if(instance.environment == null)
            System.err.println("Environment manager not initialized.");
        return instance;
    }
    
    private Environment environment;
    
    private EnvironmentManager()
    {
    
    }

    /**
     * Stores new environment
     * @param environment
     */
    public void init(Environment environment)
    {
        this.environment = environment;
    }

    /**
     * Reset stored environment
     */
    public void reset()
    {
        this.environment = null;
    }

    /**
     * Calculates the cost of a movement from a source position to a destination.
     * @param source
     * @param destination
     * @return
     */
    public int calculateMovementCost(Vector2D source, Vector2D destination)
    {
        return environment.getMap().calculateMovementCost(source, destination);
    }

    /**
     * Calculates the Height diff of a movement from a source position to a destination.
     * @param source
     * @param destination
     * @return
     */
    public int calculateClimbingHeight(Vector2D source, Vector2D destination)
    {
        Tile sourceTile = environment.getMap().getTile(source);
        Tile destinationTile = environment.getMap().getTile(destination);
        int neededClimbingHeight = destinationTile.getHeight() - sourceTile.getHeight();

        return neededClimbingHeight;
    }

    /**
     * Checks if a Tile is accessible.
     * @param position
     * @return
     */
    public boolean isTileAccessible(Vector2D position)
    {
        Tile selectedTile = environment.getMap().getTile(position);
        MapObject obstacleOnTile = environment.getMapObjectAtPosition(position);
        if (obstacleOnTile != null)
            return (selectedTile.isAccessible() && !(obstacleOnTile.getPreset().isObstacle()));
        else
            return selectedTile.isAccessible();
    }

    /**
     * Creates a new creature.
     * @param owner
     * @param targetPosition
     * @param preset
     * @return
     */
    public Creature createCreature(Player owner, Vector2D targetPosition, CreaturePreset preset)
    {
        Creature creature = new Creature(targetPosition, owner, preset);
        environment.addCreature(creature);
        return creature;
    }

    /**
     * Creates a new wizard.
     * @param owner
     * @param targetPosition
     * @param preset
     * @return
     */
    public Wizard createWizard(Player owner, Vector2D targetPosition, CreaturePreset preset)
    {
        Wizard wizard = new Wizard(targetPosition, owner, preset);
        environment.addCreature(wizard);
        return wizard;
    }

    /**
     * Creates a new map object.
     * @param position
     * @param mapObjectPreset
     * @return
     */
    public MapObject createMapObject(Vector2D position, MapObjectPreset mapObjectPreset)
    {
        FileManager fileManager = FileManager.getInstance();
        MapObject mapObject;

        if (mapObjectPreset == fileManager.getMapObjectPresets().get("ManaSource"))
            mapObject = new ManaSource(position, mapObjectPreset);
        else
            mapObject = new Obstacle(position, mapObjectPreset);
        environment.addMapObject(mapObject);
        return mapObject;
    }
}
