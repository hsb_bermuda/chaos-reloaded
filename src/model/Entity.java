package model;

import model.interfaces.IIdentifiable;

import java.io.Serializable;
import java.util.UUID;

public abstract class Entity implements IIdentifiable, Serializable
{
    private final UUID uuid;
    private Vector2D position;

    Entity(Vector2D position)
    {
        this.position = position;
        this.uuid = UUID.randomUUID();
    }

    public Vector2D getPosition()
    {
        return position;
    }

    void setPosition(Vector2D position)
    {
        this.position = position;
    }

    public UUID getUUID() { return uuid; }
}
