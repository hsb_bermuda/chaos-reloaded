package model.presets;

import javafx.scene.image.Image;
import model.Faction;

public class CreaturePreset extends PlayableObjectPreset
{
    private final int attack;
    private final int defence;
    private final int attackRange;
    private final int movementRange;
    private final int climbHeight;


    public CreaturePreset(String name, String description, int attack, int defence, int attackRange, int movementRange, int manaCost, int climbHeight, int balanceImpact, Faction faction, Image image)
    {
        super(name, description, image, manaCost, balanceImpact, faction);
        this.attack = attack;
        this.defence = defence;
        this.attackRange = attackRange;
        this.movementRange = movementRange;
        this.climbHeight = climbHeight;
    }

    public int getAttack()
    {
        return attack;
    }
    public int getDefence()
    {
        return defence;
    }
    public int getAttackRange()
    {
        return attackRange;
    }
    public int getMovementRange()
    {
        return movementRange;
    }
    public int getClimbHeight()
    {
        return climbHeight;
    }


}