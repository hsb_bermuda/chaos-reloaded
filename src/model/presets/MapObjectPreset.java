package model.presets;

import javafx.scene.image.Image;

public class MapObjectPreset extends Preset
{
    boolean isObstacle;

    public MapObjectPreset(String name, String description, Image image, boolean isObastacle)
    {
        super(name, description, image);
        this.isObstacle = isObastacle;
    }

    public boolean isObstacle()
    {
        return isObstacle;
    }
}
