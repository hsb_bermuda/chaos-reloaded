package model.presets;

import javafx.scene.image.Image;

public class TilePreset extends Preset
{
    private final String key;
    private final boolean isAccessible;

    public TilePreset(String name, String description, boolean isAccessible, String key, Image image)
    {
       super(name, description, image);
       this.isAccessible = isAccessible;
       this.key = key;
    }

    public boolean getIsAccessible()
    {
        return isAccessible;
    }
    
    public String getKey()
    {
        return key;
    }
}