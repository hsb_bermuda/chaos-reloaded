package model.presets;

import javafx.scene.image.Image;

public abstract class Preset
{
    private final String name;
    private final String description;
    private final Image image;


    Preset(String name, String description, Image image)
    {
        this.name = name;
        this.description = description;
        this.image = image;
    }
    
    public String getName()
    {
        return name;
    }
    public String getDescription()
    {
        return description;
    }
    public Image getImage() {return image;}
}
