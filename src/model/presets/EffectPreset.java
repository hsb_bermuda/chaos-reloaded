package model.presets;

import javafx.scene.image.Image;

public class EffectPreset extends Preset
{
    private final int attackValue;
    private final int range;
    private final int lastingRounds;
    
    public EffectPreset(String name, String description, int value, int range, int lastingRounds, Image image)
    {
        super(name, description, image);
        this.attackValue = value;
        this.range = range;
        this.lastingRounds = lastingRounds;
    }

    public int getAttackValue()
    {
        return attackValue;
    }
    public int getRange()
    {
        return range;
    }
    public int getLastingRounds()
    {
        return lastingRounds;
    }
}
