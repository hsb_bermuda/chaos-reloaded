package model.presets;

import javafx.scene.image.Image;
import model.Faction;

public class SpellPreset extends PlayableObjectPreset
{
    public SpellPreset(String name, String description, int manaCost, int balanceImpact, Faction faction, Image image)
    {
        super(name, description, image, manaCost, balanceImpact, faction);
    }
}
