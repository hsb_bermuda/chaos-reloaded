package model.presets;

import javafx.scene.image.Image;
import model.Faction;

public abstract class PlayableObjectPreset extends Preset
{
    private final int manaCost;
    private final int balanceImpact;
    private final Faction faction;

    PlayableObjectPreset(String name, String description, Image image, int manaCost, int balanceImpact, Faction faction)
    {
        super(name, description, image);
        this.manaCost = manaCost;
        this.balanceImpact = balanceImpact;
        this.faction = faction;
    }

    public int getManaCost()
    {
        return manaCost;
    }
    public int getBalanceImpact()
    {
        return balanceImpact;
    }
    public Faction getFaction()
    {
        return faction;
    }
}
