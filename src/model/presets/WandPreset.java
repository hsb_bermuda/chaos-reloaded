package model.presets;

import javafx.scene.image.Image;
import model.Faction;

public class WandPreset extends Preset
{
    private Faction faction;
    private int spellQuantity;

    public WandPreset(String name, String description, Image image, Faction faction, int spellQuantity)
    {
        super(name, description, image);
        this.faction = faction;
        this.spellQuantity = spellQuantity;
    }

    public Faction getFaction()
    {
        return faction;
    }
    public int getSpellQuantity()
    {
        return spellQuantity;
    }
}
