package model;

import javafx.application.Platform;
import model.cards.Card;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.IMatchEventHandler;
import model.manager.*;
import utils.MatchType;

import java.io.Serializable;
import java.util.ArrayList;

public class Match implements Serializable
{
    private ArrayList<Player> players;
    private PowerBalance powerBalance;
    private Environment environment;
    private int activePlayerIndex;
    private int completedRounds;
    private final int maxRounds;
    private MatchType matchType;

    private Creature selectedCreature;
    private Card selectedCard;

    public Match(ArrayList<Player> players, Environment environment, int maxRounds, MatchType matchType)
    {
        this.players = players;
        this.powerBalance = new PowerBalance();
        this.matchType = matchType;
        ManaManager.getInstance().init(this.powerBalance);
        this.environment = environment;
        this.completedRounds = 0;
        this.activePlayerIndex = 0;
        this.maxRounds = maxRounds;
    }

    /**
     * Is called at the end of each turn and switches to the next player in the list.
     */
    public void endTurn()
    {
        FileManager fileManager = FileManager.getInstance();
        ManaManager manaManager = ManaManager.getInstance();
        environment.activateEffectsOfWizard(getActivePlayer().getWizard());
        environment.refillLeftMovementCost();
        environment.resetHasAlreadyAttacked();
        for (Player player : players)
        {
            player.setCardAlreadyBought(false);
        }

        if (activePlayerIndex == (players.size() - 1))
        {
            setCompletedRounds(completedRounds + 1);
            if (completedRounds == maxRounds)
            {
                return;
            }
            setActivePlayerIndex(0);
            manaManager.addRoundBonusToPlayers(getPlayers(), fileManager.getConfig().getManaRoundBonus());
        } else
        {
            setActivePlayerIndex(activePlayerIndex + 1);
        }
    }

    public ArrayList<Player> getPlayers()
    {
        return players;
    }

    public PowerBalance getPowerBalance()
    {
        return powerBalance;
    }

    public Environment getEnvironment()
    {
        return environment;
    }

    public Player getActivePlayer()
    {
        return players.get(activePlayerIndex);
    }

    public int getCompletedRounds()
    {
        return completedRounds;
    }

    public Creature getSelectedCreature()
    {
        return selectedCreature;
    }

    public Card getSelectedCard()
    {
        return selectedCard;
    }

    public int getMaxRounds()
    {
        return maxRounds;
    }

    public MatchType getMatchType()
    {
        return matchType;
    }

    private void setCompletedRounds(int newValue)
    {
        completedRounds = newValue;
    }

    private void setActivePlayerIndex(int newIndex)
    {
        // TODO: Check if index is inside bounds?
        activePlayerIndex = newIndex;
    }

    public void setSelectedCard(Card selectedCard)
    {
        this.selectedCard = selectedCard;
    }

    public void setSelectedCreature(Creature selectedCreature)
    {
        this.selectedCreature = selectedCreature;
    }

}
