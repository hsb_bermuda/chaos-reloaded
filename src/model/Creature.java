package model;

import model.actionresults.MoveResult;
import model.interfaces.IDamagable;
import model.interfaces.IDamaging;
import model.interfaces.IMovable;
import model.manager.EnvironmentManager;
import model.manager.FileManager;
import model.presets.CreaturePreset;

import java.io.Serializable;

public class Creature extends Entity implements IDamagable, IDamaging, IMovable, Serializable
{
    private int health = FileManager.getInstance().getConfig().getStandardHealth();
    private Player owner;
    private String name;
    private int movementPoints;
    private int movementRange;
    private int climbHeight;
    private int attack;
    private int defence;
    private int attackRange;
    private boolean hasAlreadyAttacked;
    private boolean isDead;
    
    public Creature(Vector2D position, Player owner, CreaturePreset preset)
    {
        super(position);

        this.name = preset.getName();
        this.owner = owner;
        this.movementRange = preset.getMovementRange();
        this.attack = preset.getAttack();
        this.defence = preset.getDefence();
        this.attackRange = preset.getAttackRange();
        this.climbHeight = preset.getClimbHeight();

        setMovementPoints(movementRange);
        setHasAlreadyAttacked(false);
    }

    // IMovable Interface
    /**
     * Moving Creature to a new Vector2D.
     * implemented by IMovable interface
     *
     * @param position
     */
    @Override
    public MoveResult move(Vector2D position)
    {
        EnvironmentManager environmentManager = EnvironmentManager.getInstance();
        int neededMovementCost = environmentManager.calculateMovementCost(getPosition(), position);

        if((neededMovementCost <= getMovementPoints()) && (environmentManager.isTileAccessible(position))
                && (climbHeight >= environmentManager.calculateClimbingHeight(getPosition(), position)))
        {
            Vector2D oldPosition = getPosition();
            setPosition(position);
            setMovementPoints(getMovementPoints() - neededMovementCost);
            return new MoveResult(true, this, oldPosition, getPosition());
        }
        return new MoveResult(false, this, getPosition(), getPosition());
    }
    @Override
    public int getMovementRange()
    {
        return movementRange;
    }
    @Override
    public int getMovementPoints()
    {
        return movementPoints;
    }

    // IDamagable Interface
    /**
     * Applies damage taken to the creature. If the creature dies health is set to zero.
     *
     * @param damage
     */
    @Override
    public void receiveDamage(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            isDead = true;
        }
    }
    
    @Override
    public int getDefence()
    {
        return defence;
    }
    @Override
    public int getHealth()
    {
        return health;
    }
    @Override
    public boolean isDead()
    {
        return isDead;
    }

    // IDamaging Interface
    @Override
    public int getAttackValue()
    {
        return attack;
    }
    @Override
    public int getAttackRange()
    {
        return attackRange;
    }
    @Override
    public boolean hasAlreadyAttacked()
    {
        return hasAlreadyAttacked;
    }
    @Override
    public void setHasAlreadyAttacked(boolean hasAlreadyAttacked)
    {
        this.hasAlreadyAttacked = hasAlreadyAttacked;
    }

    public Player getOwner()
    {
        return owner;
    }
    public CreaturePreset getPreset()
    {
        return FileManager.getInstance().getCreaturePresets().get(name);
    }
    
    public void setMovementPoints(int movementPoints)
    {
        this.movementPoints = movementPoints;
    }
}
