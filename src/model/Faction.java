package model;

import java.io.Serializable;

public enum Faction implements Serializable
{
    Law,
    Neutral,
    Chaos
}