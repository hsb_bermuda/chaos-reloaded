package model.interfaces;

import utils.MatchInformation;

public interface ILobbyEventHandler extends ISubMenuEventHandler
{
    void startMatch(MatchInformation matchInformation);

    void hostMatch(MatchInformation matchInformation, String serverName, int port);
}
