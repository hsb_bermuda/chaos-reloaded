package model.interfaces;

import model.Match;

public interface ILoadingScreenEventHandler
{
    void exchangeMatch(Match match);
    void startGame();
}
