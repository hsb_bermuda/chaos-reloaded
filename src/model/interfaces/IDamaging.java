package model.interfaces;

import model.Vector2D;

/**
 * Provides functionality for an object to damage an object
 */
public interface IDamaging
{
    /**
     * Get the attack parameter of the object for calculating the damage output.
     * @return Integer based attack value.
     */
    int getAttackValue();
    
    /**
     * Gets the current position on the map.
     * @return Position on the map.
     */
    Vector2D getPosition();
    
    /**
     * Gets the attack range of the object to check if a target is in range.
     * @return Integer based range value.
     */
    int getAttackRange();
    
    /**
     * Gets a value if the object has already damaged another object in the current round.
     * @return True if the object has already damaged an object.
     */
    boolean hasAlreadyAttacked();
    
    /**
     * Sets the parameter which defines if the object already attacked another object in the current round.
     * @param hasAlreadyAttacked True if the object damaged another object in this round.
     */
    void setHasAlreadyAttacked(boolean hasAlreadyAttacked);
}
