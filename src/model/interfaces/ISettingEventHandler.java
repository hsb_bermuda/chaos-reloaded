package model.interfaces;

public interface ISettingEventHandler extends IEventHandler
{
    void backToMainMenu();
}
