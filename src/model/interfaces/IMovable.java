package model.interfaces;

import model.Vector2D;
import model.actionresults.MoveResult;

public interface IMovable
{
    MoveResult move(Vector2D position);
    
    int getMovementRange();
    Vector2D getPosition();

    int getMovementPoints();
}
