package model.interfaces;

import model.Creature;
import model.Match;
import model.Player;
import model.Vector2D;
import model.cards.Card;

public interface IMatchEventHandler extends IEventHandler
{
    /**
     * Is called when a tile is selected.
     * @param position Position selected by the user.
     */
    void onTileSelect(Vector2D position);
    
    /**
     * Is called when the selected card is changed by the user.
     * @param cardIndex New card index in the player deck.
     */
    void onCardSelect(int cardIndex);
    
    /**
     * Is called at the end of each turn.
     */
    void onEndTurn();
    
    /**
     * Requests a return to the main menu.
     */
    void goToMainMenu();
    
    /**
     * Is called when the application should be closed.
     */
    void exit();
    
    /**
     * Requests the opening of the ingame Wiki.
     */
    void goToWiki();
    
    /**
     * Resets the selection of the match.
     */
    void resetSelection();
    
    /**
     * Is called when the user enters a tile with his input device.
     * @param position Position of the tile which was entered.
     */
    void onBeginHover(Vector2D position);
    
    /**
     * Is called when selected creature is changed.
     * @param newCreature Newly selected creature.
     */
    void onSelectedCreatureChanged(Creature newCreature);
    
    /**
     * Is called when an object is attacking another suitable object.
     * @param target Target of the attack.
     * @param source Source of the attack.
     */
    void onCreatureAttack(IDamagable target, IDamaging source);
    
    /**
     * Is called when a card is to be bought by the user.
     * @param card Card to buy.
     * @param position Position where the resulting Entity should spawn.
     */
    void onCardBuy(Card card, Vector2D position);
    
    /**
     * Is called when a movable object tries to move.
     * @param creature Movable Creature.
     * @param targetPosition Destination
     */
    void onCreatureMove(IMovable creature, Vector2D targetPosition);
    
    /**
     * Is called when a creature interacts with an object on the map.
     * @param object
     * @param creature
     */
    void onMapObjectUsed(IInteractable object, Creature creature);
    
    /**
     * Is called on the end of a match.
     */
    void onMatchEnded();
    
    /**
     *
     * @param match
     */
    void exchangeMatch(Match match);
    
    /**
     * Is called when the current rounds.
     */
    void endTurn();
}
