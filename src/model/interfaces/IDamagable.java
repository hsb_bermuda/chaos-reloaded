package model.interfaces;

import model.Vector2D;

/**
 * Provides necassary functionality and fields needed for an object to be damagable.
 */
public interface IDamagable
{
    /**
     * Defines how an object is to be damaged.
     * @param damage Amount of damage to be taken.
     */
    void receiveDamage(int damage);
    
    /**
     * Checks if a creature is dead.
     * @return True if the creature is dead, false otherwise.
     */
    boolean isDead();
    
    /**
     * Gets the current health of the object.
     * @return Remaining health of the object.
     */
    int getHealth();
    
    /**
     * Gets a defence parameter for the object for calculating damage reduction.
     * @return Integer based defence value.
     */
    int getDefence();
    
    /**
     * Gets the current position of the object.
     * @return Position on the map.
     */
    Vector2D getPosition();
}
