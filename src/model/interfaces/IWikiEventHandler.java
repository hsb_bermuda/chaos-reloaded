package model.interfaces;

public interface IWikiEventHandler extends IEventHandler
{
    void goToMainMenu();
    void exit();
    void goToWiki();
}
