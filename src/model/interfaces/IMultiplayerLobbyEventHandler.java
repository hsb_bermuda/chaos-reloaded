package model.interfaces;

public interface IMultiplayerLobbyEventHandler extends ISubMenuEventHandler
{
    void startMatch();
}
