package model.interfaces;

import java.util.UUID;

public interface IIdentifiable
{
    UUID getUUID();
}
