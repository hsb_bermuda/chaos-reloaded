package model.interfaces;

/**
 * Handles events from the EndMatchScreen.
 */
public interface IEndMatchScreenEventHandler extends IEventHandler
{
    /**
     * Returns the user back to the main menu.
     */
    void backToMainMenu();
}
