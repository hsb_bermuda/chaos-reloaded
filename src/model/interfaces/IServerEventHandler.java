package model.interfaces;

import java.net.InetAddress;

public interface IServerEventHandler
{
    void clientConnected(InetAddress ip, int port);
}
