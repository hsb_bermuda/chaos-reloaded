package model.interfaces;

public interface IMainMenuEventHandler extends IEventHandler
{
    /**
     * Is called when a match should be started.
     */
    void startMatch();
    
    void openMultiplayerMenu();
    /**
     * Is called when the Wiki should be opened.
     */
    void openWiki();
    void openSettings();
    /**
     * Is called when the game should be exited.
     */
    void exitGame();
}
