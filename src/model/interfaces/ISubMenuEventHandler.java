package model.interfaces;

public interface ISubMenuEventHandler extends IEventHandler
{
    void back();
}
