package model.interfaces;

public interface IEventPublisher
{
    /**
     * Registers a IEventHandler to the publisher.
     * @param eventHandler
     */
    void registerEventHandler(IEventHandler eventHandler);

}
