package model.interfaces;

public interface IFileManagerEventHandler extends IEventHandler
{
    /**
     * Is called when the FileManager has all files loaded.
     */
    void filesLoaded();
}
