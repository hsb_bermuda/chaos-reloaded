package model.interfaces;

import model.Creature;
import model.actionresults.InteractionResult;

public interface IInteractable
{
    InteractionResult interact(Creature creature);
}
