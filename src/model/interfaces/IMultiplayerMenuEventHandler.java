package model.interfaces;

public interface IMultiplayerMenuEventHandler extends ISubMenuEventHandler
{
    void joinMatch(String address, int port);
}
