package model;

import model.manager.FileManager;
import model.presets.MapObjectPreset;

import java.io.Serializable;

public abstract class MapObject extends Entity implements Serializable
{
    private String name;
    
    MapObject(Vector2D position, MapObjectPreset preset)
    {
        super(position);
        this.name = preset.getName();
    }

    public MapObjectPreset getPreset() {
        return FileManager.getInstance().getMapObjectPresets().get(name);
    }
}
