package model;

import model.actionresults.BuyResult;
import model.cards.Card;
import model.cards.Deck;
import model.constants.Sound;
import model.manager.EnvironmentManager;
import model.manager.FileManager;
import model.manager.ManaManager;
import model.manager.SoundManager;

import java.io.Serializable;

public class Player implements Serializable
{
    private final String userName;
    private Deck deck;
    private int availableMana = 100;
    private Wizard wizard;
    private boolean cardAlreadyBought;

    public Player(String userName, Deck deck)
    {
        this.userName = userName;
        this.deck = deck;
        
        // Temporary wizard spawn.
        this.wizard = EnvironmentManager.getInstance().
                createWizard(this, Vector2D.getRandom(10, 10), FileManager.getInstance().getCreaturePresets().get("Wizard"));
        setCardAlreadyBought(false);
    }

    /**
     * Buys a card for the currently active player. No creature is spawned if the position is outside of the map or outside of the spawn range of the wizard.
     * @param card
     * @param targetPosition
     */
    public BuyResult buyCard(Card card, Vector2D targetPosition)
    {
        ManaManager manaManager = ManaManager.getInstance();
        if( (manaManager.canBuy(this, card) &&
                wizard.getPosition().distanceTo(targetPosition) <= FileManager.getInstance().getConfig().getWizardSpawnRange()) &&
                !(isCardAlreadyBought()))
        {
            int actualCardCost = manaManager.calculateActualCardCost(card);
            setAvailableMana(getAvailableMana() - actualCardCost);
            card.useCard(this, targetPosition);
            manaManager.adjustPowerBalance(card);
            getDeck().remove(card);
            setCardAlreadyBought(true);
            return new BuyResult(true, card, actualCardCost);
        }
        else
        {
            SoundManager.playSound(Sound.NOTSPAWNSOUND);
            return new BuyResult(false, card, 0);
        }
    }
    
    public String getUserName()
    {
        return userName;
    }
    public Deck getDeck()
    {
        return deck;
    }
    public int getAvailableMana()
    {
        return availableMana;
    }
    public Wizard getWizard()
    {
        return wizard;
    }
    public void setAvailableMana(int availableMana)
    {
        this.availableMana = availableMana;
    }
    public boolean isCardAlreadyBought()
    {
        return cardAlreadyBought;
    }
    public void setCardAlreadyBought(boolean cardAlreadyBought)
    {
        this.cardAlreadyBought = cardAlreadyBought;
    }
}