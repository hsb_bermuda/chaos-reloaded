package model;

import java.io.Serializable;

public class Game implements Serializable
{
    private GameState state;
    private Match match;

    public Game()
    {
    }
    
    public Match getMatch()
    {
        return match;
    }
    public GameState getState()
    {
        return state;
    }

    public void setState(GameState state)
    {
        this.state = state;
    }
    public void setMatch(Match match)
    {
        this.match = match;
    }
}
