package model.constants;

public enum SoundSetting
{
   MUSIC_VOLUME, SOUND_VOLUME
}
