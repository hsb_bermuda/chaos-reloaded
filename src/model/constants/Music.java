package model.constants;

public enum Music
{
    BACKGROUNDMUSICMAINMENU("backgroundMusicMainMenu"),
    BACKGROUNDMUSICWIKI("backgroundMusicWiki"),
    BACKGROUNDMUSICMATCH("backgroundMusicMatch");

    private String label;

    Music(String label)
    {
        this.label = label;
    }

    @Override
    public String toString()
    {
        return label;
    }
}
