package model.constants;

public enum Sound
{
    OWNMANASOUND("ownManaSound"),
    ATTACKSOUND("attackSound"),
    EFFECTSOUND("effectSound"),
    SPAWNSOUND("spawnSound"),
    NEXTTUNRSOUND("nextTurnSound"),
    VICOTRYSOUND("victorySound"),
    BUTTONCLICKSOUND("buttonClickSound"),
    NOTATTACKSOUND("notAttackSound"),
    STARTGAME("startGame"),
    NOTSPAWNSOUND("notSpawnSound");

    private String label;

    Sound(String label)
    {
        this.label = label;
    }

    @Override
    public String toString()
    {
        return label;
    }
}