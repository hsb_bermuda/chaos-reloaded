package model.constants;

import javafx.beans.property.SimpleDoubleProperty;

public interface SoundConstants
{
    SimpleDoubleProperty INITIAL_SOUND_VOLUME = new SimpleDoubleProperty(0.2);
    SimpleDoubleProperty INITAL_MUSIC_VOLUME = new SimpleDoubleProperty(0.1);
}
