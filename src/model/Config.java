package model;

import utils.MatchType;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;

import java.io.Serializable;


public class Config implements Serializable
{
    private final String defaultMap;
    private final MapSize defaultMapSize;
    private final int defaultMaxRounds;
    private final MatchType defaultMatchType;
    private final int defaultManaSourceCount;
    private final int defaultDeckSize;
    private final String wizardPreset;
    private final int wizardSpawnRange;
    private final int standardHealth;
    private final int manaRoundBonus;
    private final DoubleProperty soundVolume = new SimpleDoubleProperty();
    private final DoubleProperty musicVolume = new SimpleDoubleProperty();

    public Config(String defaultMap, String defaultMapSize, int defaultMaxRounds, String defaultMatchType, int defaultManaSourceCount, int defaultDeckSize, String wizardPreset, int wizardSpawnRange, int standardHealth, int manaRoundBonus, double soundVolume, double musicVolume) {

        this.defaultMap = defaultMap;
        switch(defaultMapSize){
            case "SMALL":
                this.defaultMapSize = MapSize.SMALL;
                break;
            case "MEDIUM":
                this.defaultMapSize = MapSize.MEDIUM;
                break;
            case "LARGE":
                this.defaultMapSize = MapSize.LARGE;
                break;
            default:
                this.defaultMapSize = MapSize.MEDIUM;
        }
        this.defaultMaxRounds = defaultMaxRounds;
        this.defaultMatchType = defaultMatchType == "LOCAL" ? MatchType.LOCAL : MatchType.ONLINE;
        this.defaultManaSourceCount = defaultManaSourceCount;
        this.defaultDeckSize = defaultDeckSize;
        this.wizardPreset = wizardPreset;
        this.wizardSpawnRange = wizardSpawnRange;
        this.standardHealth = standardHealth;
        this.manaRoundBonus = manaRoundBonus;
        this.soundVolume.setValue(soundVolume);
        this.musicVolume.setValue(musicVolume);
    }

    public String getDefaultMap()
    {
        return defaultMap;
    }

    public MapSize getDefaultMapSize()
    {
        return defaultMapSize;
    }

    public int getDefaultMaxRounds()
    {
        return defaultMaxRounds;
    }

    public MatchType getDefaultMatchType()
    {
        return defaultMatchType;
    }

    public int getDefaultManaSourceCount()
    {
        return defaultManaSourceCount;
    }

    public int getDefaultDeckSize()
    {
        return defaultDeckSize;
    }

    public String getWizardPreset()
    {
        return wizardPreset;
    }

    public int getWizardSpawnRange()
    {
        return wizardSpawnRange;
    }

    public int getStandardHealth()
    {
        return standardHealth;
    }

    public int getManaRoundBonus()
    {
        return manaRoundBonus;
    }
    public DoubleProperty getMusicVolume()
    {
        return musicVolume;
    }
    public DoubleProperty getSoundVolume()
    {
        return soundVolume;
    }
}
