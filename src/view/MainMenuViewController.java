package view;

import model.constants.Sound;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.IMainMenuEventHandler;
import model.manager.SoundManager;

public class MainMenuViewController extends ViewController implements IEventPublisher
{
    private IMainMenuEventHandler eventHandler;
    
    public MainMenuViewController() {}
    
    public void initialize() {}
    
    public void onAction_PlayButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.startMatch();
    }
    
    public void onAction_serverListButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.openMultiplayerMenu();
    }
    
    public void onAction_WikiButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.openWiki();
    }
    
    public void onAction_SettingsButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.openSettings();
    }
    
    public void onAction_ExitButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.exitGame();
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (IMainMenuEventHandler) eventHandler;
    }
}
