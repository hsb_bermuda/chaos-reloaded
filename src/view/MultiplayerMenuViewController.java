package view;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import model.constants.Sound;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.IMultiplayerMenuEventHandler;
import model.manager.SoundManager;
import networking.managers.DatabaseManager;
import networking.managers.ServerInformation;

import java.util.ArrayList;

public class MultiplayerMenuViewController extends ViewController implements IEventPublisher
{
    private IMultiplayerMenuEventHandler eventHandler;


    @FXML
    private TableView<ServerInformation> serverTable;
    
    public MultiplayerMenuViewController()
    {
    
    }

    /**
     * Called when View is initialised.
     * Refreshes Server List.
     */
    public void initialize()
    {
       refreshServerlist();
    }
    
    @FXML
    public void onAction_JoinButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        ServerInformation selectedServer = serverTable.getSelectionModel().getSelectedItem();
        if(selectedServer != null)
            eventHandler.joinMatch(selectedServer.getIp(), selectedServer.getPort());
    }
    
    @FXML
    public void onAction_RefreshButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        refreshServerlist();
    }
    
    @FXML
    public void onAction_BackButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.back();
    }

    /**
     * Refreshes Server View Table.
     * Gets Data from DatabaseManager
     */
    private void refreshServerlist() {
        ArrayList<ServerInformation> serverInformations = DatabaseManager.getInstance().getServerList();
        serverTable.setItems(FXCollections.observableArrayList(serverInformations));
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (IMultiplayerMenuEventHandler)eventHandler;
    }

}
