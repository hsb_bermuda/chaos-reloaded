package view;

import javafx.fxml.FXML;
import javafx.scene.control.Slider;
import model.constants.Sound;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.ISettingEventHandler;
import model.manager.SoundManager;

import static view.constants.ViewConstants.MAX_MUSIC_SLIDER_VALUE;
import static view.constants.ViewConstants.MAX_SOUND_SLIDER_VALUE;

public class SettingsViewController extends ViewController implements IEventPublisher
{
    private ISettingEventHandler eventHandler;

    @FXML
    Slider musicSlider;

    @FXML
    Slider soundSlider;

    public SettingsViewController()
    {
    }

    /**
     *  Called when View is initialised.
     *  Sets Max values.
     */
    public void initialize()
    {
        musicSlider.setMax(MAX_MUSIC_SLIDER_VALUE);
        soundSlider.setMax(MAX_SOUND_SLIDER_VALUE);
        musicSlider.valueProperty().bindBidirectional(SoundManager.getMusicPlayer().volumeProperty());
        soundSlider.valueProperty().bindBidirectional(SoundManager.getSoundEffectPlayer().volumeProperty());
    }

    public void onAction_backButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.backToMainMenu();
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (ISettingEventHandler) eventHandler;
    }
}
