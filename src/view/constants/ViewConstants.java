package view.constants;

public interface ViewConstants
{
     int MAX_MUSIC_SLIDER_VALUE = 1;
     int MAX_SOUND_SLIDER_VALUE = 1;
     int CARD_INNER_SHOW_RADIUS = 10;
     int MAX_DISPLAYED_POWERBALANCE_VALUE = 20;
     int CHAOS_RECTANLGE_WIDTH = 0;
     int LAW_RECTANGLE_WIDTH = 0;
     int NAME_LABLE_PREF_WIDTH = 200;
     int VALUE_LAVLE_PERF_WIDTH = 200;
}
