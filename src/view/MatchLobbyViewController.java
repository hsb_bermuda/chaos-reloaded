package view;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.layout.VBox;
import model.MapSize;
import model.constants.Sound;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.ILobbyEventHandler;
import utils.MatchInformation;
import utils.MatchType;
import model.manager.SoundManager;

import java.util.ArrayList;

public class MatchLobbyViewController extends ViewController implements IEventPublisher
{
    @FXML
    private VBox multiplayerOptions;
    
    @FXML
    private TextField maxRoundTextField;
    
    @FXML
    private ChoiceBox<MapSize> mapSizeChoiceBox;
    
    @FXML
    private TextField player1TextField;
    
    @FXML
    private TextField multiplayerPortTextField;
    
    @FXML
    private TextField player2TextField;
    
    @FXML
    private TextField manaSourcesTextField;
    
    @FXML
    private TextField deckSizeTextField;
    
    @FXML
    private TextField multiplayerNameTextField;
    
    @FXML
    private CheckBox multiplayerOptionCheckbox;

    @FXML
    private Button startButton;

    private ILobbyEventHandler eventHandler;
    
    public MatchLobbyViewController()
    {
    
    }

    /**
     * Called when View is initalised.
     * Sets Map Size choices.
     */
    public void initialize()
    {
        mapSizeChoiceBox.setItems(FXCollections.observableArrayList(MapSize.values()));
    }
    
    @FXML
    void onAction_multiplayerOptions()
    {

        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        multiplayerOptions.setDisable(!multiplayerOptionCheckbox.isSelected());
        if(multiplayerOptionCheckbox.isSelected()){
            startButton.setText("Host");
        } else {
            startButton.setText("Start Match");
        }
    }
    
    
    @FXML
    public void onAction_BackButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.back();
    }


    @FXML
    public void onAction_StartMatchButton()
    {

        SoundManager.playSound(Sound.BUTTONCLICKSOUND);

        MatchInformation matchInformation = getMatchInformationFromTextFields();

        if (multiplayerOptionCheckbox.isSelected()) {
            String serverName = multiplayerNameTextField.getText();
            int port = Integer.parseInt(multiplayerPortTextField.getText());

            eventHandler.hostMatch(matchInformation, serverName, port);
        } else {
            eventHandler.startMatch(matchInformation);
        }
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (ILobbyEventHandler) eventHandler;
    }

    /**
     * Reads Textfields and generates MatchInformation.
     * @return
     */

    private MatchInformation getMatchInformationFromTextFields() {
        ArrayList<String> userNames = new ArrayList<>();
        userNames.add(player1TextField.getText());
        userNames.add(player2TextField.getText());

        MapSize mapSize = mapSizeChoiceBox.getValue();
        int maxRounds = Integer.parseInt(maxRoundTextField.getText());
        int manaSources = Integer.parseInt(manaSourcesTextField.getText());
        int deckSize = Integer.parseInt(deckSizeTextField.getText());
        MatchType matchType;

        if (multiplayerOptionCheckbox.isSelected()) {
            matchType = MatchType.ONLINE;
        } else {
            matchType = MatchType.LOCAL;
        }

        return new MatchInformation(matchType, mapSize, maxRounds, deckSize, manaSources, userNames);
    }
}
