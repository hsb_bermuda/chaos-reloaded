package view;

import javafx.fxml.FXML;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import model.interfaces.*;
import model.manager.FileManager;
import model.presets.Preset;
import view.controls.PresetViewControl;

import java.util.Collection;

public class WikiViewController extends ViewController implements IEventPublisher
{
    @FXML
    private TreeView treeView;

    @FXML
    private PresetViewControl presetView;

    private IWikiEventHandler eventHandler;

    public WikiViewController()
    {

    }

    /**
     *  Called when View is initialised.
     *  Adds sub View notes to parent.
     */
    public void initialize()
    {
        TreeItem<String> rootItem = new TreeItem<>("Presets");
        FileManager fileManager = FileManager.getInstance();
        rootItem.getChildren().add(getTreeViewItems("Creatures", fileManager.getCreaturePresets().values()));
        rootItem.getChildren().add(getTreeViewItems("Tiles", fileManager.getTilePresets().values()));
        rootItem.getChildren().add(getTreeViewItems("Map Objects", fileManager.getMapObjectPresets().values()));
        rootItem.getChildren().add(getTreeViewItems("Effects", fileManager.getEffectPresets().values()));
        treeView.setRoot(rootItem);

        treeView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            TreeItem selectedPreset = (TreeItem) newValue;
            for (Object name : treeView.getTreeItem(treeView.getRow(selectedPreset.getParent())).getChildren())
            {
                if (newValue.equals(name))
                {
                    printSelectedPreset(selectedPreset);
                }
            }
        });
    }

    /**
     * Gets tree of model items. Those will be shown in Tree List.
     * @param title
     * @param map
     * @return TreeItem<String>
     */
    private TreeItem<String> getTreeViewItems(String title, Collection<? extends Preset> map)
    {
        TreeItem<String> presetRoot = new TreeItem<>(title);
        for (Preset preset : map)
        {
            TreeItem<String> presetItem = new TreeItem<>(preset.getName());
            presetRoot.getChildren().add(presetItem);
        }
        return presetRoot;
    }

    //TODO: update-method for every preset
    private void printSelectedPreset(TreeItem selectedPreset)
    {
        FileManager fileManager = FileManager.getInstance();
        if (selectedPreset.getParent().getValue() == "Creatures")
            presetView.update(fileManager.getCreaturePresets().get(selectedPreset.getValue()));
        else if (selectedPreset.getParent().getValue() == "Tiles")
            presetView.update(fileManager.getTilePresets().get(selectedPreset.getValue()));
        else if (selectedPreset.getParent().getValue() == "Map Objects")
            presetView.update(fileManager.getMapObjectPresets().get(selectedPreset.getValue()));
        else if (selectedPreset.getParent().getValue() == "Effects")
            presetView.update(fileManager.getEffectPresets().get(selectedPreset.getValue()));
    }

    public void onAction_endMatch()
    {
        eventHandler.goToMainMenu();
    }

    public void onAction_exit()
    {
        eventHandler.exit();
    }

    public void onAction_getToWiki()
    {
        eventHandler.goToWiki();
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (IWikiEventHandler) eventHandler;
    }
}
