package view;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import model.constants.Sound;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.IMultiplayerLobbyEventHandler;
import model.manager.SoundManager;
import utils.ClientInformation;

import java.util.ArrayList;

public class MultiplayerLobbyViewController extends ViewController implements IEventPublisher
{
    private IMultiplayerLobbyEventHandler eventHandler;

    public void initialize()
    {
    }

    @FXML
    private TableView<ClientInformation> clientTable;

    @FXML
    public void onAction_BackButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.back();
    }

    @FXML
    public void onAction_StartMatchButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.startMatch();
    }

    /**
     * Adds Client to Client View Table
     * @param clientInformation
     */
    public void addClient(ClientInformation clientInformation) {
        ArrayList<ClientInformation> clientInformations = new ArrayList<>();
        clientInformations.add(clientInformation);
        clientTable.setItems(FXCollections.observableArrayList(clientInformations));
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (IMultiplayerLobbyEventHandler) eventHandler;
    }
}
