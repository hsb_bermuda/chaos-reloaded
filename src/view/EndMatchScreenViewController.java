package view;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import model.constants.Sound;
import model.interfaces.IEndMatchScreenEventHandler;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.manager.MatchManager;
import model.manager.SoundManager;

public class EndMatchScreenViewController extends ViewController implements IEventPublisher
{
    private IEndMatchScreenEventHandler eventHandler;

    @FXML
    private Label playerWinsLabel;

    public EndMatchScreenViewController()
    {

    }

    public void initialize()
    {
        playerWinsLabel.setText(MatchManager.getInstance().getLosingPlayer().getUserName() + " lost the match.");
    }

    public void onAction_BackButton()
    {
        SoundManager.playSound(Sound.BUTTONCLICKSOUND);
        eventHandler.backToMainMenu();
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (IEndMatchScreenEventHandler) eventHandler;
    }
}
