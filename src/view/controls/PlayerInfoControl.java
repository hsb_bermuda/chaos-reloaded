package view.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import model.Player;

import java.io.IOException;

public class PlayerInfoControl extends VBox
{
    @FXML
    private Label userNameLabel;
    @FXML
    private Label availableManaLabel;
    
    public PlayerInfoControl()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PlayerInfoControl.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Updatess PlayerInfoControl
     * @param player
     */
    public void update(Player player)
    {
        userNameLabel.setText(player.getUserName());
        availableManaLabel.setText(Integer.toString(player.getAvailableMana()));
    }
}
