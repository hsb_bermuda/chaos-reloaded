package view.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import model.Match;
import model.Player;
import model.manager.FileManager;

import java.io.IOException;

public class MatchInfoControl extends AnchorPane
{
    @FXML
    private VBox playerBox;
    
    @FXML
    private Label roundLabel;
    
    @FXML
    private Label bonusLabel;
    
    public MatchInfoControl()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MatchInfoControl.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Updates MatchInfoControl
     * @param match
     */
    public void update(Match match)
    {
        roundLabel.setText(match.getCompletedRounds() + "/" + match.getMaxRounds());
        playerBox.getChildren().clear();
        for(Player player : match.getPlayers())
        {
            Label playerLabel = new Label(player.getUserName());
            playerLabel.getStyleClass().add("matchInfoLabel");
            playerBox.getChildren().add(playerLabel);
        }
        bonusLabel.setText(Integer.toString(FileManager.getInstance().getConfig().getManaRoundBonus()));
    }
}
