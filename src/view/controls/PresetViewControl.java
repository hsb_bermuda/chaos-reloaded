package view.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import model.presets.Preset;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

import static javafx.scene.paint.Color.WHITE;
import static view.constants.ViewConstants.NAME_LABLE_PREF_WIDTH;
import static view.constants.ViewConstants.VALUE_LAVLE_PERF_WIDTH;

public class PresetViewControl extends VBox
{
    @FXML
    private VBox vBox;
    @FXML
    private Label nameLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private ImageView imageView;
    
    public PresetViewControl()
    {
        URL url = getClass().getResource("PresetViewControl.fxml");
        System.out.println(url);
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Updates Preset View Control
     * @param preset
     */
    public void update(Preset preset)
    {
        vBox.getChildren().clear();

        if(preset == null)
            return;
        nameLabel.setText(preset.getName());
        descriptionLabel.setText(preset.getDescription());
        imageView.setImage(preset.getImage());

        Method[] methods = preset.getClass().getMethods();
        for (Method getterMethod : methods)
        {
            if(getterMethod.getName().startsWith("get"))
            {
                if((getterMethod.getName() == "getName") || (getterMethod.getName() == "getDescription") || (getterMethod.getName() == "getImage"))
                    return;
                try
                {
                    HBox hbox = new HBox();

                    Label nameLabel = new Label();
                    Label valueLabel = new Label();

                    String[] stringParts = getterMethod.getName().split("get");
                    nameLabel.setText(stringParts[1] + ": ");
                    valueLabel.setText(getterMethod.invoke(preset).toString());

                    nameLabel.setTextFill(WHITE);
                    nameLabel.setPrefWidth(NAME_LABLE_PREF_WIDTH);
                    valueLabel.setTextFill(WHITE);
                    valueLabel.setPrefWidth(VALUE_LAVLE_PERF_WIDTH);

                    hbox.getChildren().addAll(nameLabel, valueLabel);

                    vBox.getChildren().add(hbox);
                }
                catch (IllegalAccessException iae)
                {
                    iae.printStackTrace();
                }
                catch (InvocationTargetException ite)
                {
                    ite.printStackTrace();
                }
            }
        }
    }
}
