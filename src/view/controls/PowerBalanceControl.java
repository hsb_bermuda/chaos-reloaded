package view.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.shape.Rectangle;

import java.io.IOException;
import java.net.URL;

import static view.constants.ViewConstants.CHAOS_RECTANLGE_WIDTH;
import static view.constants.ViewConstants.LAW_RECTANGLE_WIDTH;
import static view.constants.ViewConstants.MAX_DISPLAYED_POWERBALANCE_VALUE;

public class PowerBalanceControl extends BorderPane
{
    @FXML
    private Rectangle lawRect;
    @FXML
    private Rectangle chaosRect;
    
    private double valueSizeModifier;
    

    
    public PowerBalanceControl()
    {
        URL url = getClass().getResource("PowerBalanceControl.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(url);
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        valueSizeModifier = getPrefWidth() / MAX_DISPLAYED_POWERBALANCE_VALUE;
    }

    /**
     * Updates Power Balance View Control
     * @param value
     */
    public void update(int value)
    {
        if(value >= 0)
        {
            lawRect.setWidth(value * valueSizeModifier);
            chaosRect.setWidth(CHAOS_RECTANLGE_WIDTH);
        }
        else
        {
            chaosRect.setWidth(-1 * value * valueSizeModifier);
            lawRect.setWidth(LAW_RECTANGLE_WIDTH);
        }
    }
}
