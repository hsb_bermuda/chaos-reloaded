package view.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import model.Creature;

import java.io.IOException;

public class CreatureInfoControl extends VBox
{
    @FXML
    private Label nameLabel;
    @FXML
    private Label ownerLabel;
    @FXML
    private Label descriptionLabel;
    @FXML
    private Label manaCostLabel;
    @FXML
    private Label factionLabel;
    @FXML
    private Label healthLabel;
    @FXML
    private Label attackLabel;
    @FXML
    private Label defenceLabel;
    
    public CreatureInfoControl()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CreatureInfoControl.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Updates CreatureInfoControl
     * @param creature
     */
    public void update(Creature creature)
    {
        if(creature == null)
        {
            setVisible(false);
            return;
        }
        else
            setVisible(true);
        nameLabel.setText(creature.getPreset().getName());
        ownerLabel.setText(creature.getOwner().getUserName());
        descriptionLabel.setText(creature.getPreset().getDescription());
        manaCostLabel.setText(Integer.toString(creature.getPreset().getManaCost()));
        factionLabel.setText(creature.getPreset().getFaction().toString());
        healthLabel.setText(Integer.toString(creature.getHealth()));
        attackLabel.setText(Integer.toString(creature.getAttackValue()));
        defenceLabel.setText(Integer.toString(creature.getDefence()));
    }
}
