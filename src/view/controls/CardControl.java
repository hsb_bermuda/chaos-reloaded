package view.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import model.cards.Card;

import java.io.IOException;

import static view.constants.ViewConstants.CARD_INNER_SHOW_RADIUS;

public class CardControl extends SplitPane
{
    @FXML
    private Label factionLabel;
    @FXML
    private Label costLabel;
    @FXML
    private Label nameLabel;
    @FXML
    private ImageView previewImageView;
    
    private int indexInDeck;
    private boolean isSelected;
    
    public CardControl(int cardIndex, boolean isSelected)
    {
        indexInDeck = cardIndex;
        this.isSelected = isSelected;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("CardControl.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Updates CardControl
     * @param card
     */
    public void update(Card card)
    {
        if(isSelected)
        {
            InnerShadow is = new InnerShadow(CARD_INNER_SHOW_RADIUS, Color.YELLOW);
            this.setEffect(is);
        }
        nameLabel.setText(card.getName());
        costLabel.setText(Integer.toString(card.getManaCost()));
        factionLabel.setText(card.getFaction() + ": " + (card.getPreset()).getBalanceImpact());
        previewImageView.setImage(card.getPreset().getImage());
    }
    
    public int getIndexInDeck()
    {
        return indexInDeck;
    }
}
