package view.controls;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import model.*;

import java.io.IOException;

public class TileInfoControl extends VBox
{
    @FXML
    private Label positionLabel;
    @FXML
    private Label mapObjectLabel;
    @FXML
    private Label typeLabel;
    @FXML
    private Label heightLabel;
    @FXML
    private Label descriptionLabel;
    
    public TileInfoControl()
    {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TileInfoControl.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try
        {
            fxmlLoader.load();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Updates MapObject Label
     * @param environment
     * @param position
     */
    public void update(Environment environment, Vector2D position)
    {
        Map map = environment.getMap();
        Tile tile = map.getTile(position);
        typeLabel.setText(tile.getPreset().getName());
        positionLabel.setText("[" + position.getX() + "," + position.getY() + "]");
        heightLabel.setText(Integer.toString(tile.getHeight()));
        descriptionLabel.setText(tile.getPreset().getDescription());
        MapObject object = environment.getMapObjectAtPosition(position);
        if(object != null)
            mapObjectLabel.setText(object.toString());
        else
            mapObjectLabel.setText("none");
    }
}
