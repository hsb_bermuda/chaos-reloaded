package view;

import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.effect.BlendMode;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import model.*;
import model.cards.Card;
import model.cards.Deck;
import model.constants.Sound;
import model.interfaces.IEventHandler;
import model.interfaces.IEventPublisher;
import model.interfaces.IMatchEventHandler;
import model.manager.SoundManager;
import view.controls.*;

public class MatchViewController extends ViewController implements IEventPublisher
{
    @FXML
    public PlayerInfoControl playerInfoControl;
    @FXML
    public TileInfoControl tileInfoControl;
    @FXML
    private HBox deckBox;
    @FXML
    private GridPane mapGrid;
    @FXML
    private CreatureInfoControl hoveredCreatureInfoControl;
    @FXML
    private CreatureInfoControl selectedCreatureInfoControl;
    @FXML
    private PowerBalanceControl powerBalanceControl;
    @FXML
    private MatchInfoControl matchInfoControl;
    
    private int tileWidth;
    private int tileHeigth;
    
    private IMatchEventHandler eventHandler;
    
    public MatchViewController()
    {
    }
    
    public void initialize()
    {
    }

    /**
     * Updates Powerbalance, matchInfo and playerInfo
     * Calls sub-update methods.
     *
     * @param match
     */
    public void update(Match match)
    {
        updateMap(match.getEnvironment(), match.getSelectedCreature());
        updateDeck(match.getActivePlayer().getDeck(), match.getSelectedCard());
        updateSelectedCreatureInfoControl(match.getSelectedCreature());
        powerBalanceControl.update(match.getPowerBalance().getValue());
        matchInfoControl.update(match);
        playerInfoControl.update(match.getActivePlayer());
    }

    /**
     * Iterates through map - gets Texture of specific Tile.
     * Adds EventListern to Tile.
     * Updates Map Control.
     *
     * @param environment
     * @param selectedCreature
     */
    public void updateMap(Environment environment, Creature selectedCreature)
    {
        mapGrid.getChildren().clear();
        Map map = environment.getMap();
        tileWidth = (int)mapGrid.getPrefWidth() / map.getWidth();
        tileHeigth = (int)mapGrid.getPrefHeight() / map.getHeight();
        for (int x = 0; x < map.getWidth(); x++)
        {
            for (int y = 0; y < map.getHeight(); y++)
            {
                Group tileGroup = new Group();
    
                ImageView tileTexture = new ImageView(map.getTile(x, y).getPreset().getImage());
                tileTexture.setFitWidth(tileWidth);
                tileTexture.setFitHeight(tileHeigth);
    
                tileGroup.setOnMouseClicked(event -> eventHandler.onTileSelect(getPositionInGridPane(tileGroup)));
                tileGroup.setOnMouseEntered(event ->
                {
                    tileTexture.setEffect(new InnerShadow());
                    Vector2D position = getPositionInGridPane(tileGroup);
                    eventHandler.onBeginHover(position);
                });
                tileGroup.setOnMouseExited(event -> tileTexture.setEffect(null));
                tileGroup.getChildren().add(tileTexture);
    
                // Set any enitities on the tile.
                MapObject object = environment.getMapObjectAtPosition(new Vector2D(x, y));
                if (object != null)
                {
                    addEntityTexture(tileGroup, object.getPreset().getImage());
                }
    
                Creature creature = environment.getCreatureAtPosition(new Vector2D(x, y));
                if (creature != null)
                {
                    addEntityTexture(tileGroup, creature.getPreset().getImage());
                }
                
                
                InnerShadow is = new InnerShadow();
                HighlightType hightLight = environment.getHighlight(new Vector2D(x, y), selectedCreature);
                if(hightLight != null)
                {
                    switch (hightLight)
                    {
                        case ATTACKABLE:
                            is.setColor(Color.RED);
                            break;
                        case MOVABLE:
                            is.setColor(Color.BLUE);
                            break;
                        case FRIENDLY:
                            is.setColor(Color.GREEN);
                            break;
                        case SELECTED:
                            is.setColor(Color.YELLOW);
                        default:
                            break;
                    }
                    tileGroup.setEffect(is);
                }
                tileGroup.maxHeight(tileWidth);
                tileGroup.maxWidth(tileHeigth);
                mapGrid.add(tileGroup, x, y);
            }
        }
    }

    /**
     * Sets Texture(Image) for specific Tile
     * @param tileGroup
     * @param image
     */
    private void addEntityTexture(Group tileGroup, Image image)
    {
        ImageView objectTexture = new ImageView(image);
        objectTexture.setFitWidth(tileWidth);
        objectTexture.setFitHeight(tileHeigth);
        objectTexture.setBlendMode(BlendMode.SRC_OVER);
        tileGroup.getChildren().add(objectTexture);
    }

    /**
     * Updates Card Control with new Deck.
     * @param deck
     * @param selectedCard
     */
    public void updateDeck(Deck deck, Card selectedCard)
    {
        deckBox.getChildren().clear();
        int selectedCardIndex = deck.indexOf(selectedCard);
        for (int i = 0; i < deck.size(); i++)
        {
            CardControl cardControl = new CardControl(i, (i == selectedCardIndex));
            cardControl.setOnMouseClicked(event ->
            {
                CardControl source = (CardControl)event.getSource();
                eventHandler.onCardSelect(source.getIndexInDeck());
            });
            
            cardControl.update(deck.get(i));
            deckBox.getChildren().add(cardControl);
        }
    }

    /**
     * Gets relative Position of Node in Grid Pane.
     * @param node
     * @return
     */
    private Vector2D getPositionInGridPane(Node node)
    {
        int xIndex = GridPane.getColumnIndex(node);
        int yIndex = GridPane.getRowIndex(node);
        return new Vector2D(xIndex, yIndex);
    }

    /**
     * Updates Hovered Creature Info Control
     * @param creature
     */
    public void updateHoveredCreatureInfoControl(Creature creature)
    {
        hoveredCreatureInfoControl.update(creature);
    }

    /**
     * Updates Selected Creature Info Control
     * @param creature
     */
    public void updateSelectedCreatureInfoControl(Creature creature)
    {
        selectedCreatureInfoControl.update(creature);
    }
    
    public void onAction_endTurnButton()
    {
        SoundManager.playSound(Sound.NEXTTUNRSOUND);
        eventHandler.onEndTurn();
    }

    public void onAction_endMatch()
    {
        eventHandler.goToMainMenu();
    }

    public void onAction_exit()
    {
        eventHandler.exit();
    }

    public void onAction_getToWiki()
    {
        eventHandler.goToWiki();
    }

    /**
     * Registers Event Handler. (see Java Observer Pattern)
     * @param eventHandler
     */
    @Override
    public void registerEventHandler(IEventHandler eventHandler)
    {
        this.eventHandler = (IMatchEventHandler)eventHandler;
    }

    /**
     * Updates Tile Info Control.
     * @param environment
     * @param position
     */
    public void updateTileInfo(Environment environment, Vector2D position)
    {
        tileInfoControl.update(environment, position);
    }
}
