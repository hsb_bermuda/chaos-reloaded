package utils;

import model.Map;
import model.MapSize;
import model.Tile;
import model.manager.FileManager;
import model.presets.TilePreset;

import java.util.ArrayList;
import java.util.Random;

/**
 * This class contains the map builder.
 */
public class MapGenerator
{
    public static Random rng = new Random();
    private static final int maxHeigth = 3;
    
    /**
     * Method to generated Map with given map size
     *
     * @param mapSize
     * @return
     */
    public static Map generateMap (MapSize mapSize)
    {
        FileManager fileManager = FileManager.getInstance();
        ArrayList<String> keys = new ArrayList<>(fileManager.getTilePresets().keySet());
        int keySetSize = keys.size();

        Tile[][] tileArr = new Tile[mapSize.getSize()][];
        for (int x = 0; x < tileArr.length; x++)
        {
            tileArr[x] = new Tile[mapSize.getSize()];
            for (int y = 0; y < tileArr[0].length; y++)
            {
                int height = rng.nextInt(maxHeigth);
                String rndKey = keys.get(rng.nextInt(keySetSize));
                tileArr[x][y] = new Tile(fileManager.getTilePresets().get(rndKey), height);
            }
        }
        return new Map(tileArr);
    }
}