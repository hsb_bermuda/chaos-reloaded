package utils;

import networking.commands.Command;
import networking.commands.CommandType;
import networking.payloads.*;

/**
 * This class is able to decompose command payloads and returns them in correct types.
 * This class uses explicit casts, but checks the command type beforehand.
 */

public class CommandDecomposer
{
    public AttackPayload decomposeAttackCommand(Command command) {
        if(command.getCommandType() != CommandType.ATTACK) return null;
        return (AttackPayload) command.getPayload();
    }

    public BuyCardPayload decomposeBuyCardCommand(Command command) {
        if(command.getCommandType() != CommandType.BUYCARD) return null;
        return (BuyCardPayload) command.getPayload();
    }

    public DebugPayload decomposeDebugCommand(Command command) {
        if(command.getCommandType() != CommandType.DEBUG) return null;
        return (DebugPayload) command.getPayload();
    }

    public MapObjectUsagePayload decomposeMapObjectUsageCommand(Command command) {
        if(command.getCommandType() != CommandType.MAPOBJECTUSAGE) return null;
        return (MapObjectUsagePayload) command.getPayload();
    }

    public MovePayload decomposeMoveCommand(Command command) {
        if(command.getCommandType() != CommandType.MOVE) return null;
        return (MovePayload) command.getPayload();
    }

    public StartGamePayload decomposeStartGameCommand(Command command) {
        if(command.getCommandType() != CommandType.STARTGAME) return null;
        return (StartGamePayload) command.getPayload();
    }

    public MatchPayload decomposeExchangeMatchCommand(Command command) {
        if(command.getCommandType() != CommandType.EXCHANGE_MATCH) return null;
        return (MatchPayload) command.getPayload();
    }

    public MatchPayload decomposeExchangeMatchDeltaCommand(Command command) {
        if(command.getCommandType() != CommandType.EXCHANGE_MATCH_DELTA) return null;
        return (MatchPayload) command.getPayload();
    }
}
