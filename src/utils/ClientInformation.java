package utils;

/**
 * This class is used to provide client specific informations.
 */

public class ClientInformation
{
    private final String inetAddress;
    private final int port;

    public ClientInformation(String inetAddress, int port)
    {
        this.inetAddress = inetAddress;
        this.port = port;
    }

    public String getInetAddress()
    {
        return inetAddress;
    }

    public int getPort()
    {
        return port;
    }
}
