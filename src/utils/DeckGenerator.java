package utils;

import model.cards.CreatureCard;
import model.cards.Deck;
import model.cards.SpellCard;
import model.manager.FileManager;
import model.presets.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

/**
 * This class contains the Deck builder.
 */
public class DeckGenerator
{

    /**
     * This Function is used for generating random decks.
     *
     * @param size
     */
    public static Deck generateDeck(int size, WandPreset wandPreset)
    {
        Deck deck = new Deck();
        Random generator = new Random();
        FileManager fileManager = FileManager.getInstance();

        HashMap<String, CreaturePreset> creaturePresetHashMap = fileManager.getCreaturePresets();
        HashMap<String, SpellPreset> spellPresetHashMap = fileManager.getSpellPresets();
        HashMap<String, EffectPreset> effectPresetHashMap = fileManager.getEffectPresets();

        CreaturePreset[] creaturePresets = creaturePresetHashMap.values().toArray(new CreaturePreset[0]); // cast values of creature Preset Hashmap into Array
        SpellPreset[] spellPresets = spellPresetHashMap.values().toArray(new SpellPreset[0]);
        CreaturePreset[] filteredPresets = Arrays.stream(creaturePresets).filter(x -> !x.getName().equals("Wizard")).toArray(CreaturePreset[]::new); //
        CreaturePreset[] factionFilteredCreaturePreset = Arrays.stream(filteredPresets).filter(x -> x.getFaction().equals(wandPreset.getFaction())).toArray(CreaturePreset[]::new);
        SpellPreset[] factionFilteredSpellPreset = Arrays.stream(spellPresets).filter(x -> x.getFaction().equals(wandPreset.getFaction())).toArray(SpellPreset[]::new);
        EffectPreset[] effectPresets = effectPresetHashMap.values().toArray(new EffectPreset[0]);

        for(int i = 0; i < (size - wandPreset.getSpellQuantity()); i++)
        {
            CreaturePreset[] fillCreatureDeck;
            if (i < ((size - wandPreset.getSpellQuantity()) / 2))
                fillCreatureDeck = filteredPresets;
            else
                fillCreatureDeck = factionFilteredCreaturePreset;

            int randomNumber = generator.nextInt(fillCreatureDeck.length);
            CreaturePreset randomPreset = fillCreatureDeck[randomNumber];
            deck.add(new CreatureCard(randomPreset.getName(), randomPreset));
        }

        for (int j = 0; j < wandPreset.getSpellQuantity(); j++)
        {
            SpellPreset[] fillSpellDeck;
            if (j < (wandPreset.getSpellQuantity()/2))
                fillSpellDeck = spellPresets;
            else
                fillSpellDeck = factionFilteredSpellPreset;

            int randomNumber = generator.nextInt(fillSpellDeck.length);
            SpellPreset randomPreset = fillSpellDeck[randomNumber];
            int randomEffectNumber = generator.nextInt(effectPresets.length);
            EffectPreset randomEffectPreset = effectPresets[randomEffectNumber];
            deck.add(new SpellCard(randomEffectPreset.getName(), randomPreset, randomEffectPreset));
        }

        return deck;
    }
}
