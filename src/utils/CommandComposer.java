package utils;

import model.Vector2D;
import networking.payloads.*;
import networking.commands.Command;
import networking.commands.CommandType;

import java.util.UUID;

/**
 * This class composes commands.
 * Every command got specific paramteres which match the semantics of the method names.
 */
public class CommandComposer
{
    public Command composeAttackCommand(UUID attackedEntityUUID, UUID sourceEntityUUID) {
        Command killCommand = this.composeCommand(CommandType.ATTACK, new AttackPayload(attackedEntityUUID, sourceEntityUUID));
        return killCommand;
    }

    public Command composeEndTurnCommand() {
        Command endTurnCommand = this.composeCommand(CommandType.ENDTURN, new CommandPayload());
        return endTurnCommand;
    }

    public Command composeCardBuyCommand(UUID cardUUID) {
        Command buyCardCommand = this.composeCommand(CommandType.BUYCARD, new BuyCardPayload(cardUUID));
        return buyCardCommand;
    }

    public Command composeMoveCommand(UUID creatureUUID, Vector2D position) {
        Command moveCommand = this.composeCommand(CommandType.MOVE, new MovePayload(creatureUUID, position));
        return moveCommand;
    }

    public Command composeMapObjectUsageCommand(UUID objectUUID, UUID creatureUUID) {
        Command objectUsageCommand = this.composeCommand(CommandType.MAPOBJECTUSAGE, new MapObjectUsagePayload(objectUUID, creatureUUID));
        return objectUsageCommand;
    }

    public Command composeStartGameCommand() {
        Command startGameCommand = this.composeCommand(CommandType.STARTGAME, new StartGamePayload());
        return startGameCommand;
    }

    private Command composeCommand(CommandType commandType, CommandPayload commandPayload){
        return new Command(commandType, commandPayload);
    }

}
