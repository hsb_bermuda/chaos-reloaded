package utils;

import model.MapSize;

import java.util.ArrayList;

/**
 * This class holds every relevant Information for map-creation.
 * All attributes are constant, and will be set in constructor.
 * This class guarantees that it wont be changed after init. (important for map-builder)
 */

public class MatchInformation
{
    private final ArrayList<String> usernames;
    private final MatchType matchType;
    private final MapSize mapSize;
    private final int rounds;
    private final int deckSize;
    private final int manaSourceSize;

    public MatchInformation(MatchType matchType, MapSize mapSize, int rounds, int deckSize, int manaSourceSize, ArrayList<String> usernames)
    {
        this.matchType = matchType;
        this.mapSize = mapSize;
        this.rounds = rounds;
        this.deckSize = deckSize;
        this.manaSourceSize = manaSourceSize;
        this.usernames = usernames;
    }

    public ArrayList<String> getUsernames()
    {
        return usernames;
    }

    public MatchType getMatchType()
    {
        return matchType;
    }

    public MapSize getMapSize()
    {
        return mapSize;
    }

    public int getRounds()
    {
        return rounds;
    }

    public int getDeckSize()
    {
        return deckSize;
    }

    public int getManaSourceSize()
    {
        return manaSourceSize;
    }

}
