package launcher;

import controller.SceneController;
import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application
{
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        SceneController gameStageController = new SceneController(primaryStage);
        gameStageController.runGame();
    }
}