package networking.messages;

import networking.commands.Command;

import java.net.InetAddress;
/**
 * This class provides a struct for a Message which will be send over network.
 */
public class NetworkMessage
{
    private Command command;

    private InetAddress ip;
    private int port;

    public NetworkMessage(Command command){
        this.command = command;
    }

    public NetworkMessage(Command command, InetAddress ip, int port)
    {
        this.command = command;
        this.ip = ip;
        this.port = port;
    }

    public Command getCommand()
    {
        return command;
    }

    public InetAddress getIp()
    {
        return ip;
    }

    public int getPort()
    {
        return port;
    }
}
