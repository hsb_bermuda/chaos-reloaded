package networking;

import networking.commands.Command;
import networking.interfaces.Sender;
import networking.interfaces.Receiver;
import networking.interfaces.ServerListener;
import networking.messages.NetworkMessage;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * This class contains Server specific information/methods.
 */
public class Server implements Sender
{
    private int port;
    private boolean open = true;
    private Receiver receiver;
    private ServerSocket serverSocket;
    private ServerListener serverListener;
    private ArrayList<Socket> clientSockets = new ArrayList<>();
    private ObjectInputStream in;
    private ObjectOutputStream out;


    public Server(int port, ServerListener serverListener, Receiver receiver) {
        this.receiver = receiver;
        this.serverListener = serverListener;

        createServerSocket(port);
    }

    /**
     * This method opens a server socket on a specific port.
     * @param port
     */
    public void createServerSocket(int port) {
        try {
            serverSocket = new ServerSocket(port);

            if (this.port == 0) {
                this.port = serverSocket.getLocalPort();
            } else {
                this.port = port;
            }

            createServerThread();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method creates a own thread for the server
     */
    public void createServerThread() {
        Thread serverThread = new Thread(() -> {
            while (open) {
                addClient();
            }
        });

        serverThread.setDaemon(true);
        serverThread.setName("Server");
        serverThread.start();
    }

    /**
     * This method adds client to Server.
     * Creates own Thread for client input.
     */
    public void addClient() {
        try {
            @SuppressWarnings("resource")
            final Socket clientSocket = serverSocket.accept();

            createClientThread(clientSocket);

        } catch (SocketException e) {
            serverListener.couldNotConnectToNewClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method creates own thread for clients.
     * @param clientSocket
     */
    public void createClientThread(Socket clientSocket) {
        Thread clientThread = new Thread(() -> {
            clientSockets.add(clientSocket);

            serverListener.clientConnected(clientSocket.getInetAddress(), clientSocket.getPort());

            while (open) {
                receive(clientSocket);
            }

            try {
                clientSocket.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
            clientSockets.remove(clientSocket);
        });

        clientThread.setDaemon(true);
        clientThread.setName("Client " + clientSocket.getInetAddress().toString());
        clientThread.start();
    }

    /**
     * This method recevies data passed on input stream.
     * @param clientSocket
     */
    public void receive(Socket clientSocket)
    {
        if (open)
        {
            try
            {
                setInputStream(clientSocket);

                Command streamedCommand = (Command) in.readObject();

                receiver.receivedInput(streamedCommand);
            }
            catch (Exception e)
            {
                e.printStackTrace();
                kickClient(clientSocket);
            }
        }
    }

    public void dispose() {
        open = false;

        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Socket clientSocket : clientSockets) {
            try {
                clientSocket.close();
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        }

        clientSockets.clear();
        clientSockets = null;

        serverSocket = null;

        serverListener.serverClosed();
        serverListener = null;
    }

    /**
     * This method sets input stream.
     * @param clientSocket
     */
    public void setInputStream(Socket clientSocket) {
        try {
            if (in == null) {
                in = new ObjectInputStream(clientSocket.getInputStream());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method sets output stream.
     * @param clientSocket
     */
    public void setOutputStream(Socket clientSocket) {
        try {
            if (out == null) {
                out = new ObjectOutputStream(clientSocket.getOutputStream());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method returns IP of the server socket.
     * @return
     */
    public String getIp() {
        try {
            serverSocket.getInetAddress();
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    @SuppressWarnings("resource")
    public void kickClient(Socket clientSocket) {
        serverListener.clientDisconnected(clientSocket.getInetAddress(), clientSocket.getPort());

        try {
            clientSocket.shutdownOutput();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        clientSockets.remove(clientSocket);
    }

    /**
     * This method sends messages to client(-socket).
     * @param networkMessage
     */
    @Override
    public void send(NetworkMessage networkMessage)
    {
        Socket receiver = getReceiverFromMessage(networkMessage);
        Command command = networkMessage.getCommand();

        if (receiver != null) {
            send(command, receiver);
        } else {
            send(command);
        }
    }

    /**
     * This method returns receiver from message.
     * @param networkMessage
     * @return
     */
    private Socket getReceiverFromMessage(NetworkMessage networkMessage) {
        for (Socket clientSocket : clientSockets) {
            if (clientSocket.getInetAddress() == networkMessage.getIp() && clientSocket.getPort() == networkMessage.getPort()) {
                return clientSocket;
            }
        }
        return null;
    }

    /**
     * This method sends a command to specific client.
     * @param command
     * @param clientSocket
     */
    public void send(Command command, Socket clientSocket)
    {
        if (open)
        {
            try
            {
                setOutputStream(clientSocket);

                out.writeObject(command);
                out.flush();
            }
            catch (Exception e)
            {
                e.printStackTrace();
                kickClient(clientSocket);
            }
        }
    }

    /**
     * This method sends a command to all clients.
     * @param command
     */
    public void send(Command command) {
        for (Socket clientSocket : clientSockets) {
            send(command, clientSocket);
        }
    }

}