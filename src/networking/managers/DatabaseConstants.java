package networking.managers;

/**
 * This class contains information for establishing the database connection
 */
public interface DatabaseConstants
{
    String ROOT_URL = "https://chaosreborndb.firebaseio.com/.json";
    String SERVERS_URL = "https://chaosreborndb.firebaseio.com/servers/.json";
    String NAME = "Name";
    String IP = "IP";
    String PORT = "Port";
}
