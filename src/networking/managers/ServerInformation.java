package networking.managers;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class ServerInformation
{
    private StringProperty name;
    private StringProperty ip;
    private IntegerProperty port;
    
    public ServerInformation(String name, String ip, int port)
    {
        this.name = new SimpleStringProperty(name);
        this.ip = new SimpleStringProperty(ip);
        this.port = new SimpleIntegerProperty(port);
    }
    
    public StringProperty nameProperty()
    {
        return name;
    }
    public StringProperty ipProperty()
    {
        return ip;
    }
    public IntegerProperty portProperty()
    {
        return port;
    }
    
    public String getName()
    {
        return name.get();
    }
    public int getPort()
    {
        return port.get();
    }
    public String getIp()
    {
        return ip.get();
    }
    
    public void setIp(String ip)
    {
        this.ip.set(ip);
    }
    
    public void setName(String name)
    {
        this.name.set(name);
    }
    
    public void setPort(int port)
    {
        this.port.set(port);
    }
}
