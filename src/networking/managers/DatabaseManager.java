package networking.managers;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This class manages the database accesses.
 */
public class DatabaseManager
{
    private static DatabaseManager ourInstance = new DatabaseManager();

    public static DatabaseManager getInstance()
    {
        return ourInstance;
    }

    private DatabaseManager()
    {
    }

    /**
     * This method posts a json object to the database server.
     * @param urlString
     * @param json
     */
    private void post(String urlString, JSONObject json) {
        Thread postThread = new Thread(() -> {
            try
            {
                URL url = new URL(urlString);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestProperty("X-HTTP-Method-Override", "PATCH");
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoOutput(true);

                DataOutputStream out = new DataOutputStream(connection.getOutputStream());
                out.writeBytes(json.toString());
                out.flush();
                out.close();

                int responseCode = connection.getResponseCode();
                System.out.println("\nSending 'PATCH' request to URL : " + url);
                System.out.println("Post parameters : " + json);
                System.out.println("Response Code : " + responseCode);

                BufferedReader in = new BufferedReader(
                        new InputStreamReader(connection.getInputStream()));
                String inputLine;
                StringBuilder response = new StringBuilder();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                System.out.println(response.toString());

            } catch (IOException e)
            {
                e.printStackTrace();
            }
        });
        postThread.start();
    }

    /**
     * This method gets a json object by a specific key from the database server
     * @param urlString
     * @return
     */
    private JSONObject get(String urlString) {
        try
        {
            URL url = new URL(urlString);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setDoOutput(true);

            int responseCode = connection.getResponseCode();
            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return new JSONObject(response.toString());
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    /**
     * This method adds a new server to the database.
     * @param serverInformation
     */
    public void addToServerList(ServerInformation serverInformation)
    {
        final UUID uuid = UUID.randomUUID();

        Map<String, Object> parameters = new HashMap<>();
        parameters.put(DatabaseConstants.NAME, serverInformation.getName());
        parameters.put(DatabaseConstants.IP, serverInformation.getIp());
        parameters.put(DatabaseConstants.PORT, serverInformation.getPort());

        Map<String, Object> server = new HashMap<>();
        server.put(uuid.toString(), parameters);

        JSONObject serverJSON = new JSONObject(server);

        post(DatabaseConstants.SERVERS_URL, serverJSON);
    }

    /**
     * This method gets the serverlist.
     * @return
     */
    public ArrayList<ServerInformation> getServerList()
    {
        ArrayList<ServerInformation> servers = new ArrayList<>();

        JSONObject serverListJSON = get(DatabaseConstants.SERVERS_URL);
        JSONArray serverUUIDJSONArray = serverListJSON.names();

        for (int i = 0; i < serverUUIDJSONArray.length(); i++) {
            String serverUUID = serverUUIDJSONArray.getString(i);

            JSONObject serverJSON = serverListJSON.getJSONObject(serverUUID);

            final String NAME = serverJSON.getString(DatabaseConstants.NAME);
            final String IP = serverJSON.getString(DatabaseConstants.IP);
            final int PORT = serverJSON.getInt(DatabaseConstants.PORT);

            ServerInformation server = new ServerInformation(NAME, IP, PORT);
            servers.add(server);
        }

        return servers;
    }
}
