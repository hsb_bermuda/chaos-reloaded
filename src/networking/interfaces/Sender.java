package networking.interfaces;

import networking.messages.NetworkMessage;

public interface Sender
{
    void send(NetworkMessage networkMessage);
}
