package networking.interfaces;

import java.net.InetAddress;

/**
 * This interface is used to guarantee remote method calls on the ServerListener.
 */
public interface ServerListener {
    void clientConnected(InetAddress ip, int port);

    void clientDisconnected(InetAddress ip, int port);

    void serverClosed();

    void couldNotConnectToNewClient();
}