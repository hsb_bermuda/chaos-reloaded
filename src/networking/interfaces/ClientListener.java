package networking.interfaces;

/**
 * This interface is used to guarantee remote method calls on the ClienListener.
 */
public interface ClientListener{
    void unknownHost();

    void couldNotConnect();

    void disconnected();

    void connectedToServer();
}