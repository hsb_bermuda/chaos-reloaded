package networking.interfaces;

import networking.commands.Command;

public interface Receiver {

    void receivedInput(Command command);
}
