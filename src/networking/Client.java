package networking;

import networking.commands.Command;
import networking.interfaces.ClientListener;
import networking.interfaces.Sender;
import networking.interfaces.Receiver;
import networking.messages.NetworkMessage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * This class contains Client specific information/methods.
 */
public class Client implements Sender
{
    private boolean open = true;
    private Receiver receiver;
    private Socket socket;
    private ClientListener clientListener;
    private ObjectInputStream in;
    private ObjectOutputStream out;

    
    public Client(String ip, int port, ClientListener clientListener, Receiver receiver)
    {
        this.receiver = receiver;
        this.clientListener = clientListener;

        createSocket(ip, port);
    }

    /**
     * This method opens new socket to a specific ip, port;
     * @param ip
     * @param port
     */
    private void createSocket(String ip, int port) {
        try
        {
            socket = new Socket(ip, port);

            createClientThread();
        }
        catch (UnknownHostException exception)
        {
            open = false;
            clientListener.unknownHost();
        }
        catch (IOException exception)
        {
            open = false;
            clientListener.couldNotConnect();
        }
        catch (Exception exception)
        {
            open = false;
            exception.printStackTrace();
        }
    }

    /**
     * This method creates own thread for clients.
     */
    private void createClientThread() {
        Thread clientThread = new Thread(() -> {
            while (open)
            {
                receiveObject();
            }
        });

        clientThread.setName("Client Connection");
        clientThread.setDaemon(true);
        clientThread.start();

        clientListener.connectedToServer();
    }

    /**
     * This method receives objects from server.
     */
    private void receiveObject() {
        try
        {
            setInputStream();

            Command streamedCommand = (Command) in.readObject();

            if (streamedCommand == null)
            {
                open = false;
                clientListener.disconnected();

                closeSocket();
                closeInputStream();
                closeOutputStream();
                return;
            }

            receiver.receivedInput(streamedCommand);
        }

        catch (Exception exception)
        {
            exception.printStackTrace();
            dispose();
        }
    }

    /**
     * This method inits clients input stream.
     */
    private void setInputStream() {
        try {
            if (in == null) {
                in = new ObjectInputStream(socket.getInputStream());
            }
        }
        catch (IOException exception)
        {
            dispose();
        }
    }

    /**
     * This method inits clients ouput stream.
     */
    private void setOutputStream() {
        try {
            if (out == null)
            {
                out = new ObjectOutputStream(socket.getOutputStream());
            }
        }
        catch (IOException exception)
        {
            dispose();
        }
    }

    /**
     * This method closes clients output stream.
     */
    private void closeOutputStream() {
        try
        {
            if (out != null) out.close();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * This method closes clients input stream.
     */
    private void closeInputStream() {
        try
        {
            if (in != null) in.close();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * This method closes clients socket.
     */
    private void closeSocket() {
        try
        {
            if (socket != null) socket.close();
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * This method returns the IP of client.
     * @return
     */
    public String getIp() {
        try {
            socket.getInetAddress();
            return InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * This method disposes the connection to server.
     */
    public void dispose()
    {
        try
        {
            if (open)
            {
                open = false;
                socket.close();
                in.close();
                out.close();
                clientListener.disconnected();
            }
            socket = null;
            in = null;
            out = null;
            clientListener = null;
        }
        catch (Exception exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * This method checks if client is connected.
     * @return
     */
    public boolean isConnected()
    {
        return open;
    }


    /**
     * This method sends messages to server.
     * @param networkMessage
     */
    @Override
    public void send(NetworkMessage networkMessage)
    {
        if (open)
        {
            try
            {
                setOutputStream();

                out.writeObject(networkMessage.getCommand());
                out.flush();
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}