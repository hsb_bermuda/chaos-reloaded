package networking.commands;

import java.io.Serializable;

/**
 * This class is used to define the type of a network command
 */
public enum CommandType implements Serializable
{
    SETPLAYERNAME, ATTACK, ENDTURN, MOVE, BUYCARD, MAPOBJECTUSAGE, DEBUG, STARTGAME, EXCHANGE_MATCH, EXCHANGE_MATCH_DELTA
}
