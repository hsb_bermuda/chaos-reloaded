package networking.commands;

import networking.payloads.CommandPayload;

import java.io.Serializable;

/**
 *  This class is used as a struct for child network commands.
 */
public class Command implements Serializable
{
    private final CommandType commandType;
    private final CommandPayload payload;

    public Command(CommandType commandType, CommandPayload payload){
        this.commandType = commandType;
        this.payload = payload;
    }

    public CommandType getCommandType()
    {
        return commandType;
    }

    public CommandPayload getPayload() {
        return payload;
    }
}
