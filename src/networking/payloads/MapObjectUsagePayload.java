package networking.payloads;

import java.util.UUID;

/**
 * This class contains specific payload values for an MapObjectUsageCommand.
 */
public class MapObjectUsagePayload extends CommandPayload
{
    private final UUID mapObjectUUID;
    private final UUID creatureUUID;

    public MapObjectUsagePayload(UUID mapObjectUUID, UUID creatureUUID)
    {
        this.mapObjectUUID = mapObjectUUID;
        this.creatureUUID = creatureUUID;
    }

    public UUID getCreatureUUID()
    {
        return creatureUUID;
    }

    public UUID getMapObjectUUID()
    {
        return mapObjectUUID;
    }
}
