package networking.payloads;

import model.Match;

import java.io.Serializable;

/**
 * This class contains specific payload values for an ExchangeMatchCommand.
 */
public class MatchPayload extends CommandPayload implements Serializable
{
    private final Match match;

    public MatchPayload(Match match)
    {
        this.match = match;
    }

    public Match getMatch()
    {
        return match;
    }
}
