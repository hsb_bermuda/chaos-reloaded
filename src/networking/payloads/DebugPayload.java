package networking.payloads;

import java.io.Serializable;

/**
 * This class contains specific payload values for an DebugCommand.
 */
public class DebugPayload extends CommandPayload implements Serializable
{
    private final String debugMessage;

    public DebugPayload(String debugMessage)
    {
        this.debugMessage = debugMessage;
    }

    public String getDebugMessage()
    {
        return debugMessage;
    }

}
