package networking.payloads;

import java.util.UUID;

/**
 * This class contains specific payload values for an BuyCardCommand.
 */
public class BuyCardPayload extends CommandPayload
{
    private final UUID cardUUID;

    public BuyCardPayload(UUID cardUUID)
    {
        this.cardUUID = cardUUID;
    }

    public UUID getCardUUID()
    {
        return cardUUID;
    }
}
