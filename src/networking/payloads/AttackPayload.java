package networking.payloads;

import java.util.UUID;

/**
 * This class contains specific payload values for an AttackCommand.
 */
public class AttackPayload extends CommandPayload
    {
    private final UUID attackedEntityUUID;
    private final UUID sourceEntityUUID;

    public AttackPayload(UUID attackedEntityUUID, UUID sourceEntityUUID)
    {
        this.attackedEntityUUID = attackedEntityUUID;
        this.sourceEntityUUID = sourceEntityUUID;
    }


    public UUID getAttackedEntityUUID()
    {
        return attackedEntityUUID;
    }

    public UUID getSourceEntityUUID()
    {
        return sourceEntityUUID;
    }
}
