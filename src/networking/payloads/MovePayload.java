package networking.payloads;

import model.Vector2D;

import java.util.UUID;

/**
 * This class contains specific payload values for an MovePayload.
 */
public class MovePayload extends CommandPayload
{
    private final UUID creatureUUID;
    private final Vector2D position;


    public MovePayload(UUID creatureUUID, Vector2D position)
    {
        this.creatureUUID = creatureUUID;
        this.position = position;
    }

    public UUID getCreatureUUID()
    {
        return creatureUUID;
    }

    public Vector2D getPosition()
    {
        return position;
    }
}
